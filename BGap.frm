*
* This program computes the alpha' expansion of disk integrals
* using the method described in 1609.07078. The procedures that
* do all the work are in the file BGap.h
*
        Format 250;
        Off statistics;

        #include- BGap.h

* left/right are the integration domain/integrand
* modify accordingly
        L example = Zint(1,3,4,5,8,2,6,7,[p],1,2,3,4,5,6,7,8);

* the number here is the alpha' order (allowed range 1,...,7)
* If you want the field theory limit, set it to 1
        #call BGexpand(2)

        bracket zeta2,zeta3,zeta5,zeta7;
        print +s -f;
	.end
