* Procedures for the auxiliary files used in the production of the
* paper arXiv:1609.07078,
*
* Non-abelian Z-theory: Berends-Giele recursion for the alpha'
* expansion of disk integrals
* 
* by Carlos R. Mafra and Oliver Schlotterer.
*
* You can use this file as you wish, as long as you cite the
* paper arXiv:1609.07078 afterwards
*
* Copyright (c) 2016 Carlos R. Mafra
*

        Function tL, Dynkin, Word, L, R;
        Function V, TreeLevel;
        Function tmpF,tmpF1;
        Function Mtemp, Mtemp1, Mtemp2, Mtemp3, TEMP, TEMP1, TEMP2, TEMP3;
        CFunction Ordering, MinArg;

        Symbol zwgt;
        Indices a,e,f,i,j;

* ZZ() is the calligraphic Z, ie the BG current of 1/z
* eg ZZ(1,2,3) = 1/z12z23
        CFunction Z(a),ZZ,Zint;

#procedure ZintToZZ()

* assume canonical ordering
        id Zint(1,...,'Npts',[p],?m) = Zint(?m);
        id Zint(1,?m,'Npts',?n,{'Npts'-1}) = sign_(nargs_(?m))*ZZ(1,?m)*ZZ({'Npts'-1},reverse_(?n));
        id ZZ(i?) = 1;

#endprocedure

#procedure DynkinBracket()

* Nested Lie brackets according to definition above Lemma 1.3 in
* "Lie elements and an algebra associated with shuffles", R. Ree

* Dynkin(1,2) = Word(1,2) - Word(2,1)
* Dynkin(1,2,3) = word expansion of [[1,2],3]

        if (match(Word(?m)));
        print "error: Word() already present in DynkinBracket: %t";
        exit;
        endif;

	repeat;
        id Dynkin(?m,j?,i?) = Dynkin(?m,j)*Word(i) - Word(i)*Dynkin(?m,j);
        endrepeat;
        id Dynkin(i?) = Word(i);
        chainin Word;

#endprocedure

#procedure StringTreeAmplitude(N)
*
* The local form of the string tree amplitude from 1106.2645
*
* input: TreeLevel(?m)
* output: equation (5.1) of 1603.09731 where the ordering Sigma=?m
*
* Note that the implementation below is just a hack meant to be
* used in the deriveBGcurrent-public.frm file. In particular,
* note that the integrand is always considered in the canonical order
* Do not use this function for other purposes unless you know what
* you are doing.

        if (match(tmpF(?m)));
        print "error: tmpF() already present in StringTreeAmplitude";
        exit;
        endif;

	id once TreeLevel(?m) = Ordering(?m)*tmpF(2,...,{'N'-2});
        #call Permute(tmpF)
        id tmpF(?m) = tmpF(1,?m,{'N'-1});
        #call DeconcatenateN(tmpF,2)
        id tmpF(?m1)*tmpF(?m2) = V(?m1)*V(reverse_(?m2))*Zint(?m1,'N',?m2)*sign_(nargs_(?m1)-1);

	id Ordering(?m)*Zint(?n) = Zint(?m,[p],?n);

#endprocedure

#procedure Permute(F)
*
* suggested by tueda at the FORM forum on 23.11.2015
*
* Replace F(1,2,3,...) with F(1,2,3,...) + permutations.

* There must be only one instance of F()
	if ((count('F',1) <= -1) || (count('F',1) >= 2));
		print "Error: Permute does not work for : %t";
        exit;
        endif;

	chainout 'F';
	shuffle 'F';
#endprocedure

#procedure DeconcatenateN(F,n)
*
* Deconcatenate function F into n-partitions or all partitions when n==0
*
* n==0: F(1,2,3) --> F(1)*F(2)*F(3) + F(1,2)*F(3) + F(1)*F(2,3)
* n==2: F(1,2,3) --> F(1,2)*F(3) + F(1)*F(2,3)
*
	if (match(tmpF1(?m)));
		print "Error: temp function already present: %t";
		exit;
	endif;

	if ((count('F',1) <= -1) || (count('F',1) >= 2));
		print "Error: Deconc does not work for : %t";
                exit;
        endif;

        chainout 'F';

	repeat id once 'F'(?m)*'F'(?n) =
        + 'F'(?m,?n)
        + tmpF1(?m)*'F'(?n)
        ;

        id tmpF1(?m) = 'F'(?m);

        #if 'n' == 0
* do nothing, keep all partitions
        #else
* keep only n-partitions (or up to n-partitions)
*        if ((match('F'(?m))) && (count('F',1) != 'n'));
        if ((match('F'(?m))) && (count('F',1) > 'n'));
		discard;
	endif;
	#endif

* remove trivial partition F(1,2,3) -> F(1,2,3)
	if ((count('F',1) == 1));
		discard;
	endif;

#endprocedure

#procedure ZToZZ()
*
* converts Z()*Z()*...*Z() integrals to the chain basis ZZ(...)
*
* eg Z(1,2)*Z(1,3) = ZZ(1,2,3) + ZZ(1,3,2)
*
* 1. Detect a link between Z(i,j) and ZZ(...,j,...)
* 2. Move the label j to the front using BGBasis() since ZZ() satisfies shuffle symmetry
* 3. Z(i,j)*ZZ(j,?m) = ZZ(i,j,?m)
* 4. recurse the above steps one at a time (ergo id once is used)

        id once Z(i?,j?)*Z(j?,k?) = ZZ(i,j,k);

	repeat;
        id once Z(i?,j?)*ZZ(?m,j?,?n) = Z(i,j)*ZZ(?m,a,?n)*args(a,j);
        #call BGBasis(a,ZZ)
        id ZZ(a,?m)*args(a,j?) = ZZ(j,?m);
        id Z(i?,j?)*ZZ(j?,?m) = ZZ(i,j,?m);
        endrepeat;

        #call BGBasis(,ZZ)

#endprocedure

#procedure BGBasis(i,M)

* M_{alpha,i,beta} = (-1)^|alpha| M_{i, alpha^T \shuffle beta}
* if i=0 or undefined put the lowest label first, otherwise label i

        #if 'i' != 0
* only one function contains the label i, so it's safe to use TEMP()
        id 'M'(?m,'i',?n) = Mtemp('i')*TEMP(reverse_(?m))*TEMP(?n)*sign_(nargs_(?m));
        shuffle TEMP;
        id TEMP = 1;
        id Mtemp('i')*TEMP(?m) = 'M'('i',?m);
* standalone Mtemp() with just the label i
	id Mtemp('i') = 'M'('i');
        #else
* when the label is not specified, we need to consider more than one function. Support at most 3
        id 'M'(?m1)*'M'(?m2)*'M'(?m3) = TEMP1(?m1)*TEMP2(?m2)*TEMP3(?m3);
        id 'M'(?m1)*'M'(?m2) = TEMP1(?m1)*TEMP2(?m2);
        id 'M'(?m1) = TEMP1(?m1);

        #do n = 1,3
        id TEMP'n'(?m) = TEMP'n'(?m)*MinArg(min_(?m));
        id TEMP'n'(?m,i?,?n)*MinArg(i?) = Mtemp'n'(i)*TEMP'n'(reverse_(?m))*TEMP'n'(?n)*sign_(nargs_(?m));
        shuffle TEMP'n';
        id TEMP'n' = 1;
        id Mtemp'n'(i?)*TEMP'n'(?m) = TEMP'n'(i,?m);
        id TEMP'n'(?m) = 'M'(?m);
        id Mtemp'n'(?m) = 'M'(?m);
        #enddo

        #endif

#endprocedure
