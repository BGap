id Int(2, 3, 4, 5) = Int(0, 1, z5)*Int(0, z3, z2)*Int(0, z4, z3)*Int(0, z5, z4);
id Int(2, 3, 5, 4) = Int(0, 1, z4)*Int(0, z3, z2)*Int(0, z4, z3)*Int(z4, 1, z5);
id Int(2, 4, 3, 5) = Int(0, 1, z5)*Int(0, z3, z2)*Int(0, z5, z3)*Int(z3, z5, z4);
id Int(2, 4, 5, 3) = Int(0, 1, z3)*Int(0, z3, z2)*Int(z3, 1, z5)*Int(z3, z5, z4);
id Int(2, 5, 3, 4) = Int(0, 1, z4)*Int(0, z3, z2)*Int(0, z4, z3)*Int(z4, 1, z5);

 id Int(2, 5, 4, 3) = Int(0, 1, z3)*Int(0, z3, z2)*
   Int(z3, 1, z4)*Int(z4, 1, z5);
 id Int(3, 2, 4, 5) = Int(0, 1, z5)*Int(0, z4, z2)*
   Int(0, z5, z4)*Int(z2, z4, z3);
 id Int(3, 2, 5, 4) = Int(0, 1, z4)*Int(0, z4, z2)*
   Int(z2, z4, z3)*Int(z4, 1, z5);
 id Int(3, 4, 2, 5) = Int(0, 1, z5)*Int(0, z5, z2)*
   Int(z2, z4, z3)*Int(z2, z5, z4);
 id Int(3, 4, 5, 2) = Int(0, 1, z2)*Int(z2, 1, z5)*
   Int(z2, z4, z3)*Int(z2, z5, z4);
 id Int(3, 5, 2, 4) = Int(0, 1, z4)*Int(0, z4, z2)*
   Int(z2, z4, z3)*Int(z4, 1, z5);
 id Int(3, 5, 4, 2) = Int(0, 1, z2)*Int(z2, 1, z4)*
   Int(z2, z4, z3)*Int(z4, 1, z5);
 id Int(4, 2, 3, 5) = Int(0, 1, z5)*Int(0, z3, z2)*
   Int(0, z5, z3)*Int(z3, z5, z4);
 id Int(4, 2, 5, 3) = Int(0, 1, z3)*Int(0, z3, z2)*
   Int(z3, 1, z5)*Int(z3, z5, z4);
 id Int(4, 3, 2, 5) = Int(0, 1, z5)*Int(0, z5, z2)*
   Int(z2, z5, z3)*Int(z3, z5, z4);
 id Int(4, 3, 5, 2) = Int(0, 1, z2)*Int(z2, 1, z5)*
   Int(z2, z5, z3)*Int(z3, z5, z4);
 id Int(4, 5, 2, 3) = Int(0, 1, z3)*Int(0, z3, z2)*
   Int(z3, 1, z5)*Int(z3, z5, z4);
 id Int(4, 5, 3, 2) = Int(0, 1, z2)*Int(z2, 1, z3)*
   Int(z3, 1, z5)*Int(z3, z5, z4);
 id Int(5, 2, 3, 4) = Int(0, 1, z4)*Int(0, z3, z2)*
   Int(0, z4, z3)*Int(z4, 1, z5);
 id Int(5, 2, 4, 3) = Int(0, 1, z3)*Int(0, z3, z2)*
   Int(z3, 1, z4)*Int(z4, 1, z5);
 id Int(5, 3, 2, 4) = Int(0, 1, z4)*Int(0, z4, z2)*
   Int(z2, z4, z3)*Int(z4, 1, z5);
 id Int(5, 3, 4, 2) = Int(0, 1, z2)*Int(z2, 1, z4)*
   Int(z2, z4, z3)*Int(z4, 1, z5);
 id Int(5, 4, 2, 3) = Int(0, 1, z3)*Int(0, z3, z2)*
   Int(z3, 1, z4)*Int(z4, 1, z5);
 id Int(5, 4, 3, 2) = Int(0, 1, z2)*Int(z2, 1, z3)*
   Int(z3, 1, z4)*Int(z4, 1, z5);
