* Derive the BG double current from the string disk amplitude
* using the algorithm of equation (3.31) from 1609.07078 

        Format 250;
        Off statistics;
        On highfirst;

        #include- pss2-polylogs.h
        #include- pss2-deriveBG-public.h

#procedure VVVTotL(Npts)
*
* Converts the superspace expression V_P V_Q to [Phi_P, Phi_Q] \equiv tL(P,tL(Q))
* according to equation (3.29) of 1609.07078
*
* Here tL(P) is the left-to-right Dynkin bracket of P, ell(P),
* eg tL(1,2,3) = [[1,2],3]
*
        id V(1,?m)*V({'Npts'-1},?n) = tL(1,?m,tL({'Npts'-1},?n));
        argument tL;
        id tL(i?) = i;
        endargument;

* anticommute if right branch is bigger (or equal, as it turns out) than the left
* this is used in the generation of BGphi{3,4}-ordered.h as it leads to fewer terms
*        id tL(j1?,tL(i1?,?m)) = - tmpF(i1,?m)*Dynkin(j1);
*        id tL(j1?,j2?,tL(i1?,i2?,?m)) = - tmpF(i1,i2,?m)*Dynkin(j1,j2);
*        id tL(j1?,j2?,j3?,tL(i1?,i2?,i3?,?m)) = - tmpF(i1,i2,i3,?m)*Dynkin(j1,j2,j3);
*        id tL(j1?,j2?,j3?,j4?,tL(i1?,i2?,i3?,i4?,?m)) = - tmpF(i1,i2,i3,i4,?m)*Dynkin(j1,j2,j3,j4);

	id tL(?m,tL(?n)) = tmpF(?m)*Dynkin(?n);
        #call DynkinBracket()
        id tmpF(?m)*Word(?n) = tL(?m,?n);

        transform tL replace(1,last)=(1,m1,2,m2,3,m3,4,m4,5,m5,6,m6,7,m7,8,m8,9,m9,10,m10);

#endprocedure

#procedure OrderZetaValues()
*
* This is just a trick to order the output according to
* the weight of the MZVs using the function tmpF().
* This is needed in order to (manually) add
* a few conditionals in the files BGphiN.h to load terms
* according to the weight requested by #call BGexpand(w)
*
* Note that the zwgt symbol is a trick to avoid generating
* zetas of weight higher than requested by BGexpand(w).
* This is done with the definition
* 	Symbol zwgt(:'w')
* inside BGexpand(). This definition indicates to FORM that
* it must discard any appearance of zwgt^p where p>w (good for
* performance)

        id zeta2 = zeta2*zwgt^2;
        id zeta3 = zeta3*zwgt^3;
        id zeta5 = zeta5*zwgt^5;
        id zeta7 = zeta7*zwgt^7;
        .sort
        id zwgt^x1? = tmpF(x1)*zwgt^x1;
        repeat id R(?m)*tmpF(?n) = tmpF(?n)*R(?m);
        repeat id L(?m)*tmpF(?n) = tmpF(?n)*L(?m);

#endprocedure

#procedure AmplitudeToBG(Npts)
*
* This function implements the whole conversion from the string
* tree amplitude in its (n-2)! form to the Z-theory equation of motion
* according to the rules of the \int^\eom map in section 4 of 1609.07078
*
* Notation: tL(i,j,k) = T_{Ai,Aj,Ak}^{B1,B2,B3} etc
*                     = L(i)*L(j)*L(k)*R(1)*R(2)*R(3)
*
* Note that all numeric indices indices are prefixed by 'm' in the
* end to become eg m1,m2,m3 etc. These will become the multiparticle
* labels ?m1,?m2,?m3 etc in the various BGphiN.h files (we are
* using FORM's notation for multiparticle labels: ?m), including the
* labels in the Mandelstam variables, eg
*
* s(1,2) --> KK(?m1,[c],?m2)
*
* Note that KK(m1,[c],m2) = KK(m2,[c],m1), take this into account when
* comparing the output of this procedure with some of the files in BGphiN/
*
* There is minimal editing to convert the output of this
* procedure into the files BGphiN.h (eg s/tmpF([1-9])\*// and s/\([mn]\)/?\1/g) etc)

	.sort
        L amp'Npts' = sign_({'Npts'-3})*TreeLevel(1,...,'Npts');

        #call StringTreeAmplitude('Npts')
        #call ZintToZZ()
        #call FromZZToSimpsetBasis('Npts')
        .sort
        #call VVVTotL('Npts')

        abracket tL;
        .sort
        keep brackets;
        #include- allNptJregs/all'Npts'ptJregs.h
        transform s replace(1,last)=(1,m1,2,m2,3,m3,4,m4,5,m5,6,m6,7,m7,8,m8,9,m9,10,m10);
        symmetrize s;
        .sort

        id s(i?,j?) = KK(i,[c],j);
        id tL(i1?,...,i{'Npts'-1}?) = <L(i1)>*...*<L(i{'Npts'-1})>*<R(n1)>*...*<R(n{'Npts'-1})>;
        #call OrderZetaValues()

        .sort
        L Nterms = termsin_(amp'Npts');
        bracket zeta2,zeta3,zeta5,zeta7,L,R,tL,zwgt,tmpF;
        print +s -f amp'Npts',Nterms;

#endprocedure


* The numeric value means the number of points in the string
* amplitude. Note that the BGphiN.h file is generated from the
* call with Npts=N+1 below. The currently supported values
* are Npts=4,...,10 which originate the files BGphi{3,...,9}-ordered.h

* For example, the output here gives rise to the file BGphi4-ordered.h
        #call AmplitudeToBG(8)

	.end

