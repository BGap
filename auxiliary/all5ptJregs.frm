        Format 250;
        Off statistics;

        #include- pss2-polylogs.h

#procedure IntegrateOrder(i,j)

	id order('i','j') = Int('i','j')*KN('Npts');
        #call IntegrationOrdering()
* According to Table 1 of 1609.07078, expanding the KN factor
* up to weight 3 computes the regular 5pt integrals up to zeta5.
* You can select up to weight 5 here. If you want more, you'll
* need to generate z-removal identities at higher length.
        #call KobaNielsen('Npts',3)
        #call IntegrateG(z'i')
        #call IntegrateG(z'j')

#endprocedure

#procedure Sliced(i,j)

        transform Z replace(1,last)=(1,z1,2,z2,3,z3,4,z4,5,z5,6,z6,7,z7,8,z8);

        argument Z;
        id z1 = 0;
        id z{'Npts'-1} = 1;
        endargument;
        .sort

        #call IntegrateOrder('i','j')

        .sort
        Polyfun;
        id gather(x1?) = x1;

        bracket zeta2,zeta3,zeta5,zeta7;
        print +s -f;
        .sort
        drop;
        .sort

#endprocedure

        #define Npts "5"

        L [Z(1,2)*Z(1,3)] = order(2,3)*Z(1,2)*Z(1,3);
        L [Z(1,3)*Z(2,3)] = order(2,3)*Z(1,3)*Z(2,3);
        L [Z(1,2)*Z(3,4)] = order(2,3)*Z(1,2)*Z(3,4);
        L [Z(1,3)*Z(2,4)] = order(2,3)*Z(1,3)*Z(2,4);
        #call Sliced(2,3)

        L [Z(2,4)*Z(3,4)] = order(3,2)*Z(2,4)*Z(3,4);
        L [Z(2,3)*Z(2,4)] = order(3,2)*Z(2,3)*Z(2,4);
        #call Sliced(3,2)

        .end
