* -*- Fortran -*-

        Function KN,KNtmp,prod1,prod1tmp, prod2,prod2tmp;
        CFunction G, gg, Gtmp, Int;
        CFunction sign, done;
        CFunction nargs,Arg;
        Function tz;

	CFunction arg,container, HPL;
        CFunction delta(s),order,regdone,args;
* for Polyfun trick
        CFunction gather;

        CFunction Z(a);
        CFunction s(s), Is(s), KK;

        Symbols n21,...,n88, wgt;
        Symbol der,later;
        Symbol Pi, alpha;
        Symbol zeta2,...,zeta12,[zeta5,3];
        Symbol [c],[p];
        Autodeclare Symbols x;
        Symbol pow,pow2;
        Symbol dummy;

        Indices b,c,d,k;
        Indices a,a1,...,a20;
        Indices i,i1,...,i20;
        Indices j,j1,...,j20;
        Indices m,m1,...,m20;
        Indices n,n1,...,n20;
        Indices tt,tt1,...,tt20;
        Indices w,w1,...,w20;
        Indices z,z1,...,z20;

* the worldsheet position variables
        set pos: z,z1,...,z8,tt,tt1,...,tt10;
        set bin: 0,1;

#procedure IntegrateG(z2)

	#call RemoveZlabelsCte('z2')
        #call RemoveZlabels('z2')
        #call ShuffleG('z2')

        if (match(G(?m,'z2',?n,i?)));
        multiply later;
        endif;

        if (match(G('z2',?m)));
        print "error: cannot integrate 'z2' in the first slot: %t";
        exit;
        endif;

* need this to cancel the effect of keep brackets in the z-removal ids
        .sort:beforeInt;

        if (match(later)==0);
        id Int(0,b?,'z2')*Z('z2',a1?)*G(?m,'z2') = G(a1,?m,b);
* if ids above didn't match, we have G(emptyset;z2)
        id Int(0,b?,'z2')*Z('z2',a1?) = G(a1,b);
        endif;

* we need to keep track of which variable has been integrated before applying shuffle
* regularization as the +/- i*Pi terms are affected
        id G(z?pos,?m,z?pos) = G(z,?m,z)*done(z,'z2');

* remove z in the first slot for the cases containing done()'s
        #call ShuffleRegularizeiPi()

* complain if ShuffleRegularizeiPi() didnt do its job
        if (match(G(z?pos,?m,z?pos)*done(?n)));
        print "after 'z2': %t";
        multiply later;
        endif;

	#call FinishIntegration()

#endprocedure

#procedure ShuffleRegularizeiPi()

* Keep done(z2,z3) and discard done(z3,z2) (by setting it to 1)
* This trick implements the different regularization for G(z_j;z_j) where
*
*        G(z_j;z_j) = - G(0;z_j) after integrating over z_i with z_j > z_i
*
* and
*        G(z_j;z_j) = - G(0;z_j) + iPi after integrating over z_j < z_i
*
* For example, if we get G(z3;z3)*done(z3,z2) after integrating z2 we
* shuffle regularize it as usual without i*Pi, so we set done(z3,z2) = 1
* If on the other hand we get G(z2;z2)*done(z2,z3), which means after
* we integrated z3, the shuffle regularization needs to include + i*Pi
*        id done(z3,z2) = 1;

        #do i=2,{'Npts'-2}
        #do j={'i'+1},{'Npts'-2}
        id done(z'j',z'i') = 1;
        #enddo
        #enddo
        .sort:midregfirst;

* now apply shuffle regularization with + i*Pi for terms
* containing done(). Terms that require regularization but
* contain no done() factor are regularized without i*Pi later
	#include- zremovals/zremoval-shuffle-with-iPis.h

#endprocedure


#procedure FinishIntegration()

        id Pi^2 = 6*zeta2;

        id delta(0,0) = 1;
        id delta(0,1) = 0;
        id delta(0,z4?pos) = 0;

        #call ContourSign()

*        repeat id KK(j?,[c],?m,i?) = s(i,j) + KK(j,[c],?m);
*        id KK(i?,[c]) = 0;

* Add more ids to support higher zeta-weights, currently up to zeta7
        if ((match(later) == 0));
        id G(a1?bin,1) = HPL(a1);
        id G(a1?bin,a2?bin,1) = HPL(a1,a2);
        id G(a1?bin,a2?bin,a3?bin,1) = HPL(a1,a2,a3);
        id G(a1?bin,a2?bin,a3?bin,a4?bin,1) = HPL(a1,a2,a3,a4);
        id G(a1?bin,a2?bin,a3?bin,a4?bin,a5?bin,1) = HPL(a1,a2,a3,a4,a5);
        id G(a1?bin,a2?bin,a3?bin,a4?bin,a5?bin,a6?bin,1) = HPL(a1,a2,a3,a4,a5,a6);
        id G(a1?bin,a2?bin,a3?bin,a4?bin,a5?bin,a6?bin,a7?bin,1) = HPL(a1,a2,a3,a4,a5,a6,a7);
        endif;
        #call HPLtoMZV()
        id zeta4 = 2/5*zeta2^2;
        .sort:finInt;

#endprocedure


#procedure RemoveZlabels(z1)

* Use shuffle regularization with no i*Pi
        #call EliminateFirst('z1')

* length 2
        id G('z1','z1') = - G(0,'z1');

* length 3
        id G('z1','z1','z1') = G(0,0,'z1');
        id G(a?,'z1','z1') = G(a,a,'z1') - G(0,a,'z1') - delta(a,0 )*zeta2;

        #include- zremovals/zremoval-length4.h
        #include- zremovals/zremoval-length5.h
        #include- zremovals/zremoval-length6.h
        #include- zremovals/zremoval-length7.h

#endprocedure

#procedure RemoveZlabelsCte(z1)

        #include- zremovals/zremovalCte-length7.h
        #include- zremovals/zremovalCte-length6.h
        #include- zremovals/zremovalCte-length5.h
        #include- zremovals/zremovalCte-length4.h
        #include- zremovals/zremovalCte-length3.h
        .sort:afterCte;

* length 2
        id G('z1',w2?!{,'z1'}) =
        + G(w2,'z1') + G(0,w2) - G(0,'z1')
        - i_*Pi*sign(w2,'z1')
        ;

	id delta(0,0) = 1;
        id delta(0,1) = 0;
        #call ContourSign()
        .sort:RemoveCte;

#endprocedure


#procedure EliminateFirst(z1)
*
* if the first slot is an integration variable, regularize it using Erik's
* regularization without i*Pi
*
        id G(?m) = G(?m)*nargs(nargs_(?m) - 8);

        if (match(nargs(x1?pos_)));
        print "EliminateFirst: no ids yet for G() with more than 8 arguments: %t";
        exit;
        endif;

        id nargs(i?) = 1;

* first the obvious cases using regularization
        id G('z1','z1') = - G(0,'z1');
        id G('z1','z1','z1') = + G(0,0,'z1');
        id G('z1','z1','z1','z1') = - G(0,0,0,'z1');
        id G('z1','z1','z1','z1','z1') = + G(0,0,0,0,'z1');
        id G('z1','z1','z1','z1','z1','z1') = - G(0,0,0,0,0,'z1');
        id G('z1','z1','z1','z1','z1','z1','z1') = + G(0,0,0,0,0,0,'z1');
        id G('z1','z1','z1','z1','z1','z1','z1','z1') = - G(0,0,0,0,0,0,0,'z1');


* need to make sure that ?m is not empty here, so the ids above
* must contain the ids below when ?m is empty
* 7 first labels equal
        id G('z1','z1','z1','z1','z1','z1','z1',?m,'z1') =
        - G(0,0,0,0,0,0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - G('z1','z1','z1','z1','z1','z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f1shufElim;

* 6 first labels equal
        id G('z1','z1','z1','z1','z1','z1',?m,'z1') =
        + G(0,0,0,0,0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - G('z1','z1','z1','z1','z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f2shufElim;

* 5 first labels equal
        id G('z1','z1','z1','z1','z1',?m,'z1') =
        - G(0,0,0,0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1','z1','z1','z1')*G(?m,'z1')
        - G('z1','z1','z1','z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f3shufElim;

* 4 first labels equal
        id G('z1','z1','z1','z1',?m,'z1') =
        + G(0,0,0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1','z1','z1')*G(?m,'z1')
        - G('z1','z1','z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f4shufElim;

* 3 first labels equal
        id G('z1','z1','z1',?m,'z1') =
        - G(0,0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1','z1')*G(?m,'z1')
        - G('z1','z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f5shufElim;

* 2 first labels equal
        id G('z1','z1',?m,'z1') =
        + G(0,0,'z1')*G(?m,'z1')
*        + G('z1','z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1','z1')*G(?m,'z1')
        - G('z1','z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:f6shufElim;

* 1 first label equal
        id G('z1',?m,'z1') =
        - G(0,'z1')*G(?m,'z1')
*        + G('z1','z1')*G(?m,'z1')
        - (
        + G('z1','z1')*G(?m,'z1')
        - G('z1',?m,'z1')
        );

        #call ShuffleG('z1')
        .sort:EliminateFirst;

        if (match(G('z1',?m)));
        print "EliminateFirst: 'z1' still in the first slot: %t";
        exit;
        endif;

#endprocedure

#procedure KobaNielsen(N,weight)
*
* The Koba-Nielsen expansion using polylogs
*
* 1/m!*log(z12)^m = G(0^m,z2)
* 1/m!*log(z2'N-1')^m = G(1^m,z2)
* 1/m!*log|z23|^m = 1/m!*(log(z3) - log(1 - z2/z3))^m
*                 = sum_{i=0}^m 1/(m-i)!a^(m-i)*1/i!*b^i (binomial formula)
*                 = sum_i=0^m G(0^m-i;z3)*G(z3^i;z2)
* where a = log(z3) = G(0;z3) and b = log(1-z2/z3) = G(z3;z2)
*
        .sort
        Symbol wgt(:'weight');

* eg KN(5) = x12*x13*x23*x24*x34
        id KN('N') =
        #do i=1,{'N'-2}
        #do j={'i'+1},{'N'-1}
        x'i''j'*
        #enddo
        #enddo
	dummy
        ;

        id x1{'N'-1} = 1;
        id dummy = 1;

* eg x12 -> 1 + x12*wgt + ... + x12^weight*wgt^weight
        #do i=1,{'N'-2}
        #do j={'i'+1},{'N'-1}
	id once x'i''j' = x'i''j'^'weight'*wgt^'weight' + x'i''j'^{'weight'-1}*x1^{'weight'-1};
        repeat id x'i''j'^pow2?pos_*x1^pow?pos_ = x'i''j'^pow2*wgt^pow + x'i''j'^(pow2-1)*x1^(pow-1);
        #enddo
        #enddo

        id wgt = 1;

* all variables with z_1 and z_n-1:
        #do i=2,{'N'-2}
        id once x1'i'^pow? = s(1,'i')^pow*G(gg(0,pow),z'i');
        id once x'i'{'N'-1}^pow? = s('i',{'N'-1})^pow*G(gg(1,pow),z'i');
        #enddo

* all the integration variables (they dont match above)
        #do i=2,{'N'-2}
        #do j={'i'+1},{'N'-2}
        id once x'i''j'^pow? = s('i','j')^pow*sum_(i,0,pow,G(gg(0,pow-i),z'j')*G(gg(z'j',i),z'i'));
        #enddo
        #enddo

        #if 'weight' > 8
        print "error: increase ids"
        exit;
        #endif
        bracket G;
        .sort
        keep brackets;

        id G(gg(i1?,0),?m) = 1;
        id G(gg(i1?,1),?m) = G(i1,?m);
        id G(gg(i1?,2),?m) = G(i1,i1,?m);
        id G(gg(i1?,3),?m) = G(i1,i1,i1,?m);
        id G(gg(i1?,4),?m) = G(i1,i1,i1,i1,?m);
        id G(gg(i1?,5),?m) = G(i1,i1,i1,i1,i1,?m);
        id G(gg(i1?,6),?m) = G(i1,i1,i1,i1,i1,i1,?m);
        id G(gg(i1?,7),?m) = G(i1,i1,i1,i1,i1,i1,i1,?m);
        id G(gg(i1?,8),?m) = G(i1,i1,i1,i1,i1,i1,i1,i1,?m);

        argument G;
        id z{'N'-1} = 1;
        endargument;
        .sort:endKN;

* if KobaNielsen() is called multiple times, we get multiple
* nesting of gather(). The trick below avoids this:
        .sort
        Polyfun;
        id gather(x1?) = x1;
        .sort:endtrick;

* the above guarantees that no gather() is around, so do the
* polyfun trick. This is fundamental for performance!
        abracket s;
        .sort
        collect gather;
        .sort
        polyfun gather;

#endprocedure


#procedure ContourSign()
*
* Southampton, 2.7.2016, 
*
        id once sign(i?,j?)^2 = 1;

        #do i=2,{'Npts'-2}
        id once sign(z'i',1) = 1;
        id once sign(1,z'i') = - 1;
        #do j={'i'+1},{'Npts'-2}
        id once sign(z'i',z'j') = 1;
        id once sign(z'j',z'i') = - 1;
        #enddo
        #enddo

#endprocedure

#procedure IntegrationOrdering()

        id Int(2) = Int(0,1,z2);

        id Int(2,3) = Int(0,1,z3)*Int(0,z3,z2);
        id Int(3,2) = Int(0,1,z2)*Int(z2,1,z3);

        id Int(2,3,4) = Int(0,1,z4)*Int(0,z4,z3)*Int(0,z3,z2);
        id Int(2,4,3) = Int(0,1,z3)*Int(z3,1,z4)*Int(0,z3,z2);
        id Int(3,2,4) = Int(0,1,z4)*Int(z2,z4,z3)*Int(0,z4,z2);
        id Int(3,4,2) = Int(0,1,z2)*Int(z2,1,z4)*Int(z2,z4,z3);
        id Int(4,2,3) = Int(0,1,z3)*Int(0,z3,z2)*Int(z3,1,z4);
        id Int(4,3,2) = Int(0,1,z2)*Int(z2,1,z3)*Int(z3,1,z4);

        #include- order/order7.h
        #include- order/order8.h
        #include- order/order9.h
        #include- order/order10.h

        id Int(z1?pos,z2?pos,z3?pos) = Int(0,z2,z3) - Int(0,z1,z3);
        id Int(z1?pos,1,z3?pos) = Int(0,1,z3) - Int(0,z1,z3);

#endprocedure


#procedure FromZZToSimpsetBasis(Npts)
*
* The input must come from TreeLevelAmplitude() such that the functions are
* ordered as ZZ(1,P)*ZZ(npts-1,Q)
*
        #if 'Npts' == 4
        id ZZ(i?,j?) = Z(i,j);
        #elseif 'Npts' == 5
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(2,4)
        #elseif 'Npts' == 6
* distance = 3
        #call StripOffZ(1,4)
        #call StripOffZ(2,5)
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(2,4)
        #call StripOffZ(3,5)
        #elseif 'Npts' == 7
* distance = 4
        #call StripOffZ(1,5)
        #call StripOffZ(6,2)
* distance = 3
        #call StripOffZ(1,4)
        #call StripOffZ(5,2)
        #call StripOffZ(6,3)
        #call StripOffZ(5,2)
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(4,2)
        #call StripOffZ(5,3)
        #call StripOffZ(6,4)
        #elseif 'Npts' == 8
* distance = 5
        #call StripOffZ(1,6)
        #call StripOffZ(2,7)
* distance = 4
        #call StripOffZ(1,5)
        #call StripOffZ(2,6)
        #call StripOffZ(3,7)
* distance = 3
        #call StripOffZ(1,4)
        #call StripOffZ(2,5)
        #call StripOffZ(3,6)
        #call StripOffZ(4,7)
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(2,4)
        #call StripOffZ(3,5)
        #call StripOffZ(4,6)
        #call StripOffZ(5,7)
        #elseif 'Npts' == 9
* distance = 6
        #call StripOffZ(1,7)
        #call StripOffZ(2,8)
* distance = 5
        #call StripOffZ(1,6)
        #call StripOffZ(2,7)
        #call StripOffZ(3,8)
* distance = 4
        #call StripOffZ(1,5)
        #call StripOffZ(2,6)
        #call StripOffZ(3,7)
        #call StripOffZ(4,8)
* distance = 3
        #call StripOffZ(1,4)
        #call StripOffZ(2,5)
        #call StripOffZ(3,6)
        #call StripOffZ(4,7)
        #call StripOffZ(5,8)
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(2,4)
        #call StripOffZ(3,5)
        #call StripOffZ(4,6)
        #call StripOffZ(5,7)
        #call StripOffZ(6,8)
        #elseif 'Npts' == 10
* distance = 7
        #call StripOffZ(1,8)
        #call StripOffZ(2,9)
* distance = 6
        #call StripOffZ(1,7)
        #call StripOffZ(2,8)
        #call StripOffZ(3,9)
* distance = 5
        #call StripOffZ(1,6)
        #call StripOffZ(2,7)
        #call StripOffZ(3,8)
        #call StripOffZ(4,9)
* distance = 4
        #call StripOffZ(1,5)
        #call StripOffZ(2,6)
        #call StripOffZ(3,7)
        #call StripOffZ(4,8)
        #call StripOffZ(5,9)
* distance = 3
        #call StripOffZ(1,4)
        #call StripOffZ(2,5)
        #call StripOffZ(3,6)
        #call StripOffZ(4,7)
        #call StripOffZ(5,8)
        #call StripOffZ(6,9)
* distance = 2
        #call StripOffZ(1,3)
        #call StripOffZ(2,4)
        #call StripOffZ(3,5)
        #call StripOffZ(4,6)
        #call StripOffZ(5,7)
        #call StripOffZ(6,8)
        #call StripOffZ(7,9)
        #endif

#endprocedure

#procedure FromSimpsetToZZBasis(Npts)

	#call ZToZZ()
        .sort:firstZZ;
        #call ZToZZ()
        .sort:secZZ;
        #call BGBasis(1,ZZ)
        .sort
        #call BGBasis({'Npts'-1},ZZ)

#endprocedure

#procedure StripOffZ(i,j)

* strips off z_ij from a chain ZZ(P,i,Q,j,R) using shuffle ids
* such that ZZ(P,i,Q,j,R) = z_ij*(...)

        #call StripRank8('i','j')
        #call StripRank8('j','i')
        id ZZ('i','j',k?,?m) = Z('i','j')*ZZ('j',k,?m);
        id ZZ('j','i',k?,?m) = Z('j','i')*ZZ('i',k,?m);

        #call StripRank7('i','j')
        #call StripRank7('j','i')
        id ZZ('i','j',k?,?m) = Z('i','j')*ZZ('j',k,?m);
        id ZZ('j','i',k?,?m) = Z('j','i')*ZZ('i',k,?m);

        #call StripRank6('i','j')
        #call StripRank6('j','i')
        id ZZ('i','j',k?,?m) = Z('i','j')*ZZ('j',k,?m);
        id ZZ('j','i',k?,?m) = Z('j','i')*ZZ('i',k,?m);

        #call StripRank5('i','j')
        #call StripRank5('j','i')
        id ZZ('i','j',k?,?m) = Z('i','j')*ZZ('j',k,?m);
        id ZZ('j','i',k?,?m) = Z('j','i')*ZZ('i',k,?m);

        #call StripRank4('i','j')
        #call StripRank4('j','i')
        id ZZ('i','j',k?,?m) = Z('i','j')*ZZ('j',k,?m);
        id ZZ('j','i',k?,?m) = Z('j','i')*ZZ('i',k,?m);

        #call StripRank3('i','j')
        #call StripRank3('j','i')
        id ZZ(k?,'i','j') = Z(k,'i')*Z('i','j');
        id ZZ(k?,'j','i') = Z(k,'j')*Z('j','i');
        id ZZ('i','j',k?) = Z('i','j')*Z('j',k);
        id ZZ('j','i',k?) = Z('j','i')*Z('i',k);

        id ZZ(i?,j?) = Z(i,j);

#endprocedure

#procedure StripRank8(i,j)

        id ZZ('i',a?,b?,c?,d?,e?,f?,'j') =
        - ZZ('i',a,b,c,d,e,'j',f)
        - ZZ('i',a,b,c,d,'j',e,f)
        - ZZ('i',a,b,c,'j',d,e,f)
        - ZZ('i',a,b,'j',c,d,e,f)
        - ZZ('i',a,'j',b,c,d,e,f)
        - ZZ('i','j',a,b,c,d,e,f)
        - ZZ('j','i',a,b,c,d,e,f)
        ;

	id ZZ('i',a?,b?,c?,d?,e?,'j',f?) = ZZ('i',a,b,c,d,e,'j')*ZZ('j',f);
	id ZZ('i',a?,b?,c?,d?,'j',e?,f?) = ZZ('i',a,b,c,d,'j')*ZZ('j',e,f);
        id ZZ('i',a?,b?,c?,'j',d?,e?,f?) = ZZ('i',a,b,c,'j')*ZZ('j',d,e,f);
        id ZZ('i',a?,b?,'j',c?,d?,e?,f?) = ZZ('i',a,b,'j')*ZZ('j',c,d,e,f);
        id ZZ('i',a?,'j',b?,c?,d?,e?,f?) = ZZ('i',a,'j')*ZZ('j',b,c,d,e,f);

#endprocedure

#procedure StripRank7(i,j)

        id ZZ('i',a?,b?,c?,d?,e?,'j') =
        - ZZ('i',a,b,c,d,'j',e)
        - ZZ('i',a,b,c,'j',d,e)
        - ZZ('i',a,b,'j',c,d,e)
        - ZZ('i',a,'j',b,c,d,e)
        - ZZ('i','j',a,b,c,d,e)
        - ZZ('j','i',a,b,c,d,e)
        ;

	id ZZ('i',a?,b?,c?,d?,'j',e?) = ZZ('i',a,b,c,d,'j')*ZZ('j',e);
        id ZZ('i',a?,b?,c?,'j',d?,e?) = ZZ('i',a,b,c,'j')*ZZ('j',d,e);
        id ZZ('i',a?,b?,'j',c?,d?,e?) = ZZ('i',a,b,'j')*ZZ('j',c,d,e);
        id ZZ('i',a?,'j',b?,c?,d?,e?) = ZZ('i',a,'j')*ZZ('j',b,c,d,e);

#endprocedure

#procedure StripRank6(i,j)

        id ZZ('i',a?,b?,c?,d?,'j') =
        - ZZ('i',a,b,c,'j',d)
        - ZZ('i',a,b,'j',c,d)
        - ZZ('i',a,'j',b,c,d)
        - ZZ('i','j',a,b,c,d)
        - ZZ('j','i',a,b,c,d)
        ;

        id ZZ('i',a?,b?,c?,'j',d?) = ZZ('i',a,b,c,'j')*ZZ('j',d);
        id ZZ('i',a?,b?,'j',c?,d?) = ZZ('i',a,b,'j')*ZZ('j',c,d);
        id ZZ('i',a?,'j',b?,c?,d?) = ZZ('i',a,'j')*ZZ('j',b,c,d);

#endprocedure

#procedure StripRank5(i,j)

        id ZZ('i',a?,b?,c?,'j') = - ZZ('i',a,b,'j',c) - ZZ('i',a,'j',b,c) - ZZ('i','j',a,b,c) - ZZ('j','i',a,b,c);
        id ZZ('i',a?,b?,'j',c?) = ZZ('i',a,b,'j')*ZZ('j',c);
        id ZZ('i',a?,'j',b?,c?) = ZZ('i',a,'j')*ZZ('j',b,c);

#endprocedure

#procedure StripRank4(i,j)

        id ZZ('i',a?,b?,'j') = - ZZ('i',a,'j',b) - ZZ('i','j',a,b) - ZZ('j','i',a,b);
        id ZZ('i',a?,'j',b?) = ZZ('i',a,'j')*Z('j',b);

#endprocedure

#procedure StripRank3(i,j)

        id ZZ('i',a?,'j') = - ZZ('j','i',a) - ZZ(a,'j','i');

#endprocedure


#procedure Zremoval()

	if (match(G(?m,z1?pos,?n,z1?pos)*G(?p,z2?pos,?q,z2?pos)));
        print "error: more than one polylog in %t";
        exit;
        endif;

* detect if there are zs to remove
        id G(?m,z1?,?n,z1?) = G(?m,z1,?n,z1)*Arg(z1);

* add the constant term
        id G(?m)*Arg(i?) = G(?m)*Arg(i) + Greg(?m);

* substitute repeated zs by t_i, where i=pos(z1) inside G
        repeat id G(?m,z1?,?n)*Arg(z1?) = G(?m,tz(nargs_(?m)+1),?n)*Arg(z1);

* put derivative symbols for each tz
        #do i=2,8
        id G(?m,tz('i'),?n) =
        + Gtmp(?m,der,t'i',?n)
        + G(?m,t'i',?n)
        ;
        id Gtmp(?m,tz('i'),?n) = Gtmp(?m,t'i',?n);
        #enddo
        id Gtmp(?m) = G(?m);

* remove terms with no derivatives generated in the last step above
        if ((match(G(?m,der,?n))==0) && (match(Greg(?m))==0));
        discard;
        endif;
	.sort

* derivative in the last entry, eq 5.25
        id G(a?,?m,der,t?) = Z(t,a)*G(?m,t);

* derivative in the penultimate entry, eq 5.26, a=a_(n-1), tz(i)=a_n, tz(j)=z
        id G(?m,a?,der,t?,b?) =
        + Z(a,t)*G(?m,t,b)
        + Z(t,a)*G(?m,a,b)
        - Z(t,0)*G(?m,a,b)
        ;

* derivative in the middle entry, eq 5.24.
* There must be at least 2 labels to the right of the tz being derivated, so
* that we are guaranteed to not be the last or penultimate entry
        id G(?m,a?,der,t?,b?,c?,?n) =
        + Z(a,t)*G(?m,t,b,c,?n)
        + Z(t,b)*G(?m,a,t,c,?n)
        - Z(t,b)*G(?m,a,b,c,?n)
        + Z(t,a)*G(?m,a,b,c,?n)
        ;

        argument;
        #do i=2,8
        id t'i' = t;
        #enddo
        endargument;

* remove z's with lower-length ids
        #call RemoveZlabels(t)
        if (match(G(?m,t,?n,t)));
        print "error: incomplete z removal, %t";
        exit;
        endif;

	.sort
        id Arg(z1?) = Int(0,z1,t);
        #call IntegrateG(t)

#endprocedure

#procedure ShuffleG(z)

	repeat;
        id G(?m,'z')*G(?n,'z') =
        + container(G(?m)*G(?n)*arg('z'));

        argument container;
        shuffle G;
        id G(?m)*arg('z') = G(?m,'z');
        endargument;

        id container(x1?) = x1;
        endrepeat;

#endprocedure

#procedure HPLtoMZV()

* Despite the name, HPL(a,b,c) here is G(a,b;c) from 1304.7267 (note the ;)
* This is NOT the harmonic polylog!

        id once HPL(0) = 0;
        id once HPL(0,0) = 0;
        id once HPL(0,0,0) = 0;
        id once HPL(0,0,0,0) = 0;
        id once HPL(0,0,0,0,0) = 0;
        id once HPL(0,0,0,0,0,0) = 0;
        id once HPL(0,0,0,0,0,0,0) = 0;
        id once HPL(0,0,0,0,0,0,0,0) = 0;

        id once HPL(1) = 0;
        id once HPL(1,1) = 0;
        id once HPL(1,1,1) = 0;
        id once HPL(1,1,1,1) = 0;
        id once HPL(1,1,1,1,1) = 0;
        id once HPL(1,1,1,1,1,1) = 0;
        id once HPL(1,1,1,1,1,1,1) = 0;
        id once HPL(1,1,1,1,1,1,1,1) = 0;

* w = 2
        id HPL(0,1) = - zeta2;
        id HPL(1,0) = + zeta2;

* w = 3
        id HPL(1,0,0) = - zeta3;
        id HPL(0,1,0) = 2*zeta3;
        id HPL(0,0,1) = - zeta3;
        id HPL(1,1,0) = + zeta3;
        id HPL(1,0,1) = - 2*zeta3;
        id HPL(0,1,1) = + zeta3;

* w = 4
        id HPL(1,0,0,0) = zeta4;
        id HPL(1,1,1,0) = zeta4;
        id HPL(0,1,0,0) = -3*zeta4;
        id HPL(1,1,0,1) = -3*zeta4;
        id HPL(0,0,1,0) = +3*zeta4;
        id HPL(1,0,1,1) = +3*zeta4;
        id HPL(0,0,0,1) = - zeta4;
        id HPL(0,1,1,1) = - zeta4;

        id HPL(1,1,0,0) = - 1/4*zeta4;
        id HPL(1,0,1,0) = + 7/4*zeta4;
        id HPL(1,0,0,1) = - 5/4*zeta4;
        id HPL(0,1,1,0) = - 5/4*zeta4;
        id HPL(0,1,0,1) = + 3/4*zeta4;
        id HPL(0,0,1,1) = + 1/4*zeta4;

        #include- HPL/HPLrules5.h
        #include- HPL/HPLrules6.h
        #include- HPL/HPLrules7.h
        #include- HPL/HPLrules8.h

#endprocedure
