****************
* obtained in recursiveZlabels.frm
****************

        bracket G;
        .sort:braclen5;
        keep brackets;

* length 5: 5 zs

   id G(a1?!{,'z1'},'z1','z1','z1','z1') =
       - delta(0,a1)*zeta4
       + delta(0,a1)*G(a1,'z1')*zeta3
       - delta(0,a1)*G(0,'z1')*zeta3
       + G(a1,a1,a1,a1,'z1')
       - G(a1,a1,0,a1,'z1')
       - G(a1,0,a1,a1,'z1')
       + G(a1,0,0,a1,'z1')
       - G(0,a1,a1,a1,'z1')
       + G(0,a1,0,a1,'z1')
       + G(0,0,a1,a1,'z1')
       - G(0,0,0,a1,'z1')
      ;

   id G(a1?!{,'z1'},a2?!{,'z1'},'z1','z1','z1') =
       + 1/4*delta(0,a1)*delta(0,a2)*zeta4
       - delta(0,a1)*delta(0,a2)*G(a2,'z1')*zeta3
       + delta(0,a1)*delta(0,a2)*G(0,'z1')*zeta3
       + delta(0,a1)*G(a2,a2,'z1')*zeta2
       - delta(0,a1)*G(a2,'z1')*zeta3
       - delta(0,a1)*G(0,a2,'z1')*zeta2
       + delta(0,a2)*G(a1,'z1')*zeta3
       - delta(0,a2)*G(a2,a1,'z1')*zeta2
       + delta(0,a2)*G(0,a1,'z1')*zeta2
       + G(a1,a2,a2,a2,'z1')
       - G(a1,a2,0,a2,'z1')
       - G(a1,0,a2,a2,'z1')
       + G(a1,0,0,a2,'z1')
       - G(a2,a1,a1,a1,'z1')
       + G(a2,a1,a2,a2,'z1')
       + G(a2,a1,0,a1,'z1')
       - G(a2,a1,0,a2,'z1')
       - G(a2,a2,a1,a1,'z1')
       + G(a2,a2,a1,a2,'z1')
       + G(a2,a2,0,a1,'z1')
       + G(a2,0,a1,a1,'z1')
       - G(a2,0,a1,a2,'z1')
       - G(a2,0,0,a1,'z1')
       - G(0,a1,a2,a2,'z1')
       + G(0,a1,0,a2,'z1')
       + G(0,a2,a1,a1,'z1')
       - G(0,a2,a1,a2,'z1')
       - G(0,a2,0,a1,'z1')
       + G(0,0,a1,a2,'z1')
      ;

   id G(a1?!{,'z1'},'z1',a2?!{,'z1'},'z1','z1') =
       + 3/4*delta(0,a1)*delta(0,a2)*zeta4
       - delta(0,a1)*delta(0,a2)*G(a1,'z1')*zeta3
       + 3*delta(0,a1)*delta(0,a2)*G(a2,'z1')*zeta3
       - 2*delta(0,a1)*delta(0,a2)*G(0,'z1')*zeta3
       + delta(0,a1)*G(a1,a2,'z1')*zeta2
       - 2*delta(0,a1)*G(a2,a2,'z1')*zeta2
       + delta(0,a1)*G(0,a2,'z1')*zeta2
       - delta(0,a2)*G(a1,a1,'z1')*zeta2
       + delta(0,a2)*G(a2,a1,'z1')*zeta2
       + G(a1,a1,a2,a2,'z1')
       - G(a1,a1,0,a2,'z1')
       - G(a1,a2,a1,a1,'z1')
       + G(a1,a2,a1,a2,'z1')
       + G(a1,a2,0,a1,'z1')
       - G(a1,0,a1,a2,'z1')
       + G(a2,a1,a1,a2,'z1')
       - G(a2,a1,a2,a2,'z1')
       + G(a2,a1,0,a2,'z1')
       + 2*G(a2,a2,a1,a1,'z1')
       - 2*G(a2,a2,a1,a2,'z1')
       - 2*G(a2,a2,0,a1,'z1')
       + G(a2,0,a1,a2,'z1')
       - G(0,a1,a1,a2,'z1')
       - G(0,a2,a1,a1,'z1')
       + G(0,a2,a1,a2,'z1')
       + G(0,a2,0,a1,'z1')
      ;

   id G(a1?!{,'z1'},'z1','z1',a2?!{,'z1'},'z1') =
       - 5/4*delta(0,a1)*delta(0,a2)*zeta4
       + 2*delta(0,a1)*delta(0,a2)*G(a1,'z1')*zeta3
       - 2*delta(0,a1)*delta(0,a2)*G(a2,'z1')*zeta3
       - delta(0,a1)*G(a1,a2,'z1')*zeta2
       + delta(0,a1)*G(a2,a2,'z1')*zeta2
       + delta(0,a1)*G(a2,'z1')*zeta3
       + G(a1,a1,a1,a2,'z1')
       + G(a1,a2,a1,a1,'z1')
       - G(a1,a2,a1,a2,'z1')
       - G(a1,a2,0,a1,'z1')
       + G(a2,a1,a1,a1,'z1')
       - G(a2,a1,a1,a2,'z1')
       - G(a2,a1,0,a1,'z1')
       - G(a2,a2,a1,a1,'z1')
       + G(a2,a2,a1,a2,'z1')
       + G(a2,a2,0,a1,'z1')
       - G(a2,0,a1,a1,'z1')
       + G(a2,0,0,a1,'z1')
      ;

   id G(a1?!{,'z1'},a2?!{,'z1'},a3?!{,'z1'},'z1','z1') =
       - delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta4
       + delta(0,a1)*delta(0,a2)*G(a3,'z1')*zeta3
       - delta(0,a1)*G(a3,a2,'z1')*zeta2
       - delta(0,a2)*delta(0,a3)*G(a1,'z1')*zeta3
       + delta(0,a2)*G(a1,a3,'z1')*zeta2
       + delta(0,a2)*G(a3,a1,'z1')*zeta2
       - delta(0,a3)*G(a1,a2,'z1')*zeta2
       + G(a1,a2,a3,a3,'z1')
       - G(a1,a2,0,a3,'z1')
       - G(a1,a3,a2,a2,'z1')
       + G(a1,a3,a2,a3,'z1')
       + G(a1,a3,0,a2,'z1')
       - G(a1,0,a2,a3,'z1')
       - G(a3,a1,a2,a2,'z1')
       + G(a3,a1,a2,a3,'z1')
       + G(a3,a1,0,a2,'z1')
       + G(a3,a2,a1,a1,'z1')
       - G(a3,a2,a1,a2,'z1')
       - G(a3,a2,0,a1,'z1')
       + G(a3,0,a1,a2,'z1')
       - G(0,a1,a2,a3,'z1')
      ;

   id G(a1?!{,'z1'},a2?!{,'z1'},'z1',a3?!{,'z1'},'z1') =
       + 3*delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta4
       - delta(0,a1)*delta(0,a2)*G(a3,'z1')*zeta3
       - 2*delta(0,a1)*delta(0,a3)*G(a2,'z1')*zeta3
       + delta(0,a1)*G(a2,a3,'z1')*zeta2
       + delta(0,a1)*G(a3,a2,'z1')*zeta2
       + 2*delta(0,a2)*delta(0,a3)*G(a1,'z1')*zeta3
       - delta(0,a2)*G(a1,a3,'z1')*zeta2
       - delta(0,a2)*G(a3,a1,'z1')*zeta2
       + G(a1,a2,a2,a3,'z1')
       + G(a1,a3,a2,a2,'z1')
       - G(a1,a3,a2,a3,'z1')
       - G(a1,a3,0,a2,'z1')
       - G(a2,a1,a1,a3,'z1')
       + G(a2,a1,a2,a3,'z1')
       - G(a2,a3,a1,a1,'z1')
       + G(a2,a3,a1,a3,'z1')
       + G(a2,a3,0,a1,'z1')
       + G(a3,a1,a2,a2,'z1')
       - G(a3,a1,a2,a3,'z1')
       - G(a3,a1,0,a2,'z1')
       - G(a3,a2,a1,a1,'z1')
       + G(a3,a2,a1,a2,'z1')
       + G(a3,a2,0,a1,'z1')
       - G(a3,0,a1,a2,'z1')
      ;

   id G(a1?!{,'z1'},'z1',a2?!{,'z1'},a3?!{,'z1'},'z1') =
       - 3*delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta4
       + 2*delta(0,a1)*delta(0,a3)*G(a2,'z1')*zeta3
       - delta(0,a1)*G(a2,a3,'z1')*zeta2
       + G(a1,a1,a2,a3,'z1')
       + G(a2,a1,a1,a3,'z1')
       - G(a2,a1,a2,a3,'z1')
       + G(a2,a3,a1,a1,'z1')
       - G(a2,a3,a1,a3,'z1')
       - G(a2,a3,0,a1,'z1')
      ;

        id delta(0,0) = 1;
        id once delta(0,1) = 0;
       .sort:len5;
