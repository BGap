
        id G(z?pos,z?pos,z?pos,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/6*(- G(0,z) + i_*Pi)*G(z,z,z,z,z,z)*done(z,i)
	;

* 5 initial zs
	id G(z?pos,z?pos,z?pos,z?pos,z?pos,a1?,z?pos)*done(z?,i?) =
	+ 1/5*(- G(0,z) + i_*Pi)*G(z,z,z,z,a1,z)*done(z,i)
	- 1/5*G(z,z,z,z,a1,z,z)*done(z,i)
	;

* 4 initial zs
	id G(z?pos,z?pos,z?pos,z?pos,a1?,z?pos,z?pos)*done(z?,i?) =
	+ 1/4*(- G(0,z) + i_*Pi)*G(z,z,z,a1,z,z)*done(z,i)
	- 1/2*G(z,z,z,a1,z,z,z)*done(z,i)
	;

	id G(z?pos,z?pos,z?pos,z?pos,a1?,a2?,z?pos)*done(z?,i?) =
	+ 1/4*(- G(0,z) + i_*Pi)*G(z,z,z,a1,a2,z)*done(z,i)
	- 1/4*G(z,z,z,a1,a2,z,z)*done(z,i)
	- 1/4*G(z,z,z,a1,z,a2,z)*done(z,i)
	;

* 3 initial zs
	id G(z?pos,z?pos,z?pos,a1?,z?pos,a2?,z?pos)*done(z?,i?) =
	+ 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,z,a2,z)*done(z,i)
	- 1/3*G(z,z,a1,z,a2,z,z)*done(z,i)
	- 2/3*G(z,z,a1,z,z,a2,z)*done(z,i)
	;

	id G(z?pos,z?pos,z?pos,a1?,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,z,z,z)*done(z,i)
	- G(z,z,a1,z,z,z,z)*done(z,i)
	;

	id G(z?pos,z?pos,z?pos,a1?,a2?,a3?,z?pos)*done(z?,i?) =
	+ 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,a2,a3,z)*done(z,i)
	- 1/3*G(z,z,a1,a2,a3,z,z)*done(z,i)
	- 1/3*G(z,z,a1,a2,z,a3,z)*done(z,i)
	- 1/3*G(z,z,a1,z,a2,a3,z)*done(z,i)
	;

	id G(z?pos,z?pos,z?pos,a1?,a2?,z?pos,z?pos)*done(z?,i?) =
	+ 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,a2,z,z)*done(z,i)
	- 2/3*G(z,z,a1,a2,z,z,z)*done(z,i)
	- 1/3*G(z,z,a1,z,a2,z,z)*done(z,i)
	;

* 2 initial zs
	id G(z?pos,z?pos,a1?,a2?,a3?,a4?,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,a2,a3,a4,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 1/2*G(z,a1,a2,a3,a4,z,z)*done(z,i)
	- 1/2*G(z,a1,a2,a3,z,a4,z)*done(z,i)
	- 1/2*G(z,a1,a2,z,a3,a4,z)*done(z,i)
	- 1/2*G(z,a1,z,a2,a3,a4,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,a2?,a3?,z?pos,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,a2,a3,z,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- G(z,a1,a2,a3,z,z,z)*done(z,i)
	- 1/2*G(z,a1,a2,z,a3,z,z)*done(z,i)
	- 1/2*G(z,a1,z,a2,a3,z,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,a2?,z?pos,a3?,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,a2,z,a3,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 1/2*G(z,a1,a2,z,a3,z,z)*done(z,i)
	- G(z,a1,a2,z,z,a3,z)*done(z,i)
	- 1/2*G(z,a1,z,a2,z,a3,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,a2?,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,a2,z,z,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 3/2*G(z,a1,a2,z,z,z,z)*done(z,i)
	- 1/2*G(z,a1,z,a2,z,z,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,z?pos,a2?,a3?,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,z,a2,a3,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 1/2*G(z,a1,z,a2,a3,z,z)*done(z,i)
	- 1/2*G(z,a1,z,a2,z,a3,z)*done(z,i)
	- G(z,a1,z,z,a2,a3,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,z?pos,a2?,z?pos,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,z,a2,z,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- G(z,a1,z,a2,z,z,z)*done(z,i)
	- G(z,a1,z,z,a2,z,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,z?pos,z?pos,a2?,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,z,z,a2,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 1/2*G(z,a1,z,z,a2,z,z)*done(z,i)
	- 3/2*G(z,a1,z,z,z,a2,z)*done(z,i)
	;

	id G(z?pos,z?pos,a1?,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/2*G(z,a1,z,z,z,z)*(- G(0,z) + i_*Pi)*done(z,i)
	- 2*G(z,a1,z,z,z,z,z)*done(z,i)
	;

* 1 initial z
	id G(z?pos,a1?,a2?,a3?,a4?,a5?,z?pos)*done(z?,i?) =
	+ G(a1,a2,a3,a4,a5,z)*(- G(0,z) + i_*Pi)
	- G(a1,a2,a3,a4,a5,z,z)
	- G(a1,a2,a3,a4,z,a5,z)
	- G(a1,a2,a3,z,a4,a5,z)
	- G(a1,a2,z,a3,a4,a5,z)
	- G(a1,z,a2,a3,a4,a5,z)
	;

	id G(z?pos,a1?,a2?,a3?,a4?,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,a2,a3,a4,z,z)*(- G(0,z) + i_*Pi)
	- 2*G(a1,a2,a3,a4,z,z,z)
	- G(a1,a2,a3,z,a4,z,z)
	- G(a1,a2,z,a3,a4,z,z)
	- G(a1,z,a2,a3,a4,z,z)
	;

	id G(z?pos,a1?,a2?,a3?,z?pos,a4?,z?pos)*done(z?,i?) =
	+ G(a1,a2,a3,z,a4,z)*(- G(0,z) + i_*Pi)
	- G(a1,a2,a3,z,a4,z,z)
	- 2*G(a1,a2,a3,z,z,a4,z)
	- G(a1,a2,z,a3,z,a4,z)
	- G(a1,z,a2,a3,z,a4,z)
	;

	id G(z?pos,a1?,a2?,a3?,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,a2,a3,z,z,z)*(- G(0,z) + i_*Pi)
	- 3*G(a1,a2,a3,z,z,z,z)
	- G(a1,a2,z,a3,z,z,z)
	- G(a1,z,a2,a3,z,z,z)
	;

	id G(z?pos,a1?,a2?,z?pos,a3?,a4?,z?pos)*done(z?,i?) =
	+ G(a1,a2,z,a3,a4,z)*(- G(0,z) + i_*Pi)
	- G(a1,a2,z,a3,a4,z,z)
	- G(a1,a2,z,a3,z,a4,z)
	- 2*G(a1,a2,z,z,a3,a4,z)
	- G(a1,z,a2,z,a3,a4,z)
	;

	id G(z?pos,a1?,a2?,z?pos,a3?,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,a2,z,a3,z,z)*(- G(0,z) + i_*Pi)
	- 2*G(a1,a2,z,a3,z,z,z)
	- 2*G(a1,a2,z,z,a3,z,z)
	- G(a1,z,a2,z,a3,z,z)
	;

	id G(z?pos,a1?,a2?,z?pos,z?pos,a3?,z?pos)*done(z?,i?) =
	+ G(a1,a2,z,z,a3,z)*(- G(0,z) + i_*Pi)
	- G(a1,a2,z,z,a3,z,z)
	- 3*G(a1,a2,z,z,z,a3,z)
	- G(a1,z,a2,z,z,a3,z)
	;

	id G(z?pos,a1?,a2?,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,a2,z,z,z,z)*(- G(0,z) + i_*Pi)
	- 4*G(a1,a2,z,z,z,z,z)
	- G(a1,z,a2,z,z,z,z)
	;

	id G(z?pos,a1?,z?pos,a2?,a3?,a4?,z?pos)*done(z?,i?) =
	+ G(a1,z,a2,a3,a4,z)*(- G(0,z) + i_*Pi)
	- G(a1,z,a2,a3,a4,z,z)
	- G(a1,z,a2,a3,z,a4,z)
	- G(a1,z,a2,z,a3,a4,z)
	- 2*G(a1,z,z,a2,a3,a4,z)
	;

	id G(z?pos,a1?,z?pos,a2?,a3?,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,z,a2,a3,z,z)*(- G(0,z) + i_*Pi)
	- 2*G(a1,z,a2,a3,z,z,z)
	- G(a1,z,a2,z,a3,z,z)
	- 2*G(a1,z,z,a2,a3,z,z)
	;

	id G(z?pos,a1?,z?pos,a2?,z?pos,a3?,z?pos)*done(z?,i?) =
	+ G(a1,z,a2,z,a3,z)*(- G(0,z) + i_*Pi)
	- G(a1,z,a2,z,a3,z,z)
	- 2*G(a1,z,a2,z,z,a3,z)
	- 2*G(a1,z,z,a2,z,a3,z)
	;

	id G(z?pos,a1?,z?pos,a2?,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,z,a2,z,z,z)*(- G(0,z) + i_*Pi)
	- 3*G(a1,z,a2,z,z,z,z)
	- 2*G(a1,z,z,a2,z,z,z)
	;

	id G(z?pos,a1?,z?pos,z?pos,a2?,a3?,z?pos)*done(z?,i?) =
	+ G(a1,z,z,a2,a3,z)*(- G(0,z) + i_*Pi)
	- G(a1,z,z,a2,a3,z,z)
	- G(a1,z,z,a2,z,a3,z)
	- 3*G(a1,z,z,z,a2,a3,z)
	;

	id G(z?pos,a1?,z?pos,z?pos,a2?,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,z,z,a2,z,z)*(- G(0,z) + i_*Pi)
	- 2*G(a1,z,z,a2,z,z,z)
	- 3*G(a1,z,z,z,a2,z,z)
	;

	id G(z?pos,a1?,z?pos,z?pos,z?pos,a2?,z?pos)*done(z?,i?) =
	+ G(a1,z,z,z,a2,z)*(- G(0,z) + i_*Pi)
	- G(a1,z,z,z,a2,z,z)
	- 4*G(a1,z,z,z,z,a2,z)
	;

	id G(z?pos,a1?,z?pos,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ G(a1,z,z,z,z,z)*(- G(0,z) + i_*Pi)
	- 5*G(a1,z,z,z,z,z,z)
        ;

* length 6, 0 a
	id G(z?pos,z?pos,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/5*(- G(0,z) + i_*Pi)*G(z,z,z,z,z)*done(z,i)
	;

* length 6, 1 a
	id G(z?pos,z?pos,z?pos,z?pos,a1?,z?pos)*done(z?,i?) =
	+ 1/4*(- G(0,z) + i_*Pi)*G(z,z,z,a1,z)*done(z,i)
	- 1/4*G(z,z,z,a1,z,z)*done(z,i)
	;

        id G(z?pos,z?pos,z?pos,a1?,z?pos,z?pos)*done(z?,i?) =
	+ 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,z,z)*done(z,i)
	- 2/3*G(z,z,a1,z,z,z)*done(z,i)
        ;

        id G(z?pos,z?pos,a1?,z?pos,z?pos,z?pos)*done(z?,i?) =
	+ 1/2*(- G(0,z) + i_*Pi)*G(z,a1,z,z,z)*done(z,i)
        - 3/2*G(z,a1,z,z,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,z,z,z)
        - 4*G(a1,z,z,z,z,z)
        ;

* length 6, 2 a
        id G(z?pos,z?pos,z?pos,a1?,a2?,z?pos)*done(z?,i?) =
        + 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,a2,z)*done(z,i)
        - 1/3*G(z,z,a1,z,a2,z)*done(z,i)
        - 1/3*G(z,z,a1,a2,z,z)*done(z,i)
        ;

        id G(z?pos,z?pos,a1?,z?pos,a2?,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,z,a2,z)*done(z,i)
        - G(z,a1,z,z,a2,z)*done(z,i)
        - 1/2*G(z,a1,z,a2,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,z?pos,a2?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,z,a2,z)
        - 3*G(a1,z,z,z,a2,z)
        - G(a1,z,z,a2,z,z)
        ;

        id G(z?pos,z?pos,a1?,a2?,z?pos,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,a2,z,z)*done(z,i)
        - 1/2*G(z,a1,z,a2,z,z)*done(z,i)
        - G(z,a1,a2,z,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,a2?,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,a2,z,z)
        - 2*G(a1,z,z,a2,z,z)
        - 2*G(a1,z,a2,z,z,z)
        ;

        id G(z?pos,a1?,a2?,z?pos,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,z,z,z)
        - G(a1,z,a2,z,z,z)
        - 3*G(a1,a2,z,z,z,z)
        ;

        id G(z?pos,z?pos,a1?,a2?,a3?,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,a2,a3,z)*done(z,i)
        - 1/2*G(z,a1,z,a2,a3,z)*done(z,i)
        - 1/2*G(z,a1,a2,z,a3,z)*done(z,i)
        - 1/2*G(z,a1,a2,a3,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,a2?,a3?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,a2,a3,z)
        - 2*G(a1,z,z,a2,a3,z)
        - G(a1,z,a2,z,a3,z)
        - G(a1,z,a2,a3,z,z)
        ;

        id G(z?pos,a1?,a2?,z?pos,a3?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,z,a3,z)
        - G(a1,z,a2,z,a3,z)
        - 2*G(a1,a2,z,z,a3,z)
        - G(a1,a2,z,a3,z,z)
        ;

        id G(z?pos,a1?,a2?,a3?,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,a3,z,z)
        - G(a1,z,a2,a3,z,z)
        - G(a1,a2,z,a3,z,z)
        - 2*G(a1,a2,a3,z,z,z)
        ;

        id G(z?pos,a1?,a2?,a3?,a4?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,a3,a4,z)
        - G(a1,z,a2,a3,a4,z)
        - G(a1,a2,z,a3,a4,z)
        - G(a1,a2,a3,z,a4,z)
        - G(a1,a2,a3,a4,z,z)
        ;

* length 5, 0 a
        id G(z?pos,z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
        + 1/4*(- G(0,z) + i_*Pi)*G(z,z,z,z)*done(z,i);

* length 5, 1 a
        id G(z?pos,z?pos,z?pos,a1?,z?pos)*done(z?,i?) =
        + 1/3*(- G(0,z) + i_*Pi)*G(z,z,a1,z)*done(z,i)
        - 1/3*G(z,z,a1,z,z)*done(z,i)
        ;

        id G(z?pos,z?pos,a1?,z?pos,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,z,z)*done(z,i)
        - G(z,a1,z,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,z,z)
        - 3*G(a1,z,z,z,z)
        ;

* length 5, 2 a
        id G(z?pos,z?pos,a1?,a2?,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,a2,z)*done(z,i)
        - 1/2*G(z,a1,z,a2,z)*done(z,i)
        - 1/2*G(z,a1,a2,z,z)*done(z,i)
        ;

        id G(z?pos,a1?,z?pos,a2?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,a2,z)
        - 2*G(a1,z,z,a2,z)
        - G(a1,z,a2,z,z)
        ;

        id G(z?pos,a1?,a2?,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,z,z)
        - G(a1,z,a2,z,z)
        - 2*G(a1,a2,z,z,z)
        ;

* length 5, 3 a
        id G(z?pos,a1?,a2?,a3?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,a3,z)
        - G(a1,z,a2,a3,z)
        - G(a1,a2,z,a3,z)
        - G(a1,a2,a3,z,z)
        ;

* length 4
        id G(z?pos,z?pos,z?pos,z?pos)*done(z?,i?) =
        + 1/3*(- G(0,z) + i_*Pi)*G(z,z,z)*done(z,i);

        id G(z?pos,z?pos,a1?,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*G(z,a1,z)*done(z,i)
        - 1/2*G(z,a1,z,z)*done(z,i);

        id G(z?pos,a1?,z?pos,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,z,z)
        - 2*G(a1,z,z,z);

        id G(z?pos,a1?,a2?,z?pos)*done(z?,i?) =
        + (- G(0,z) + i_*Pi)*G(a1,a2,z)
        - G(a1,z,a2,z)
        - G(a1,a2,z,z);

* length 3
        id G(z?pos,z?pos,z?pos)*done(z?,i?) =
        + 1/2*(- G(0,z) + i_*Pi)*(- G(0,z) + i_*Pi);
        id G(z?pos,a1?,z?pos)*done(z?,i?) = (- G(0,z) + i_*Pi)*G(a1,z) - G(a1,z,z);

        id G(z?pos,z?pos)*done(z?,i?) = - G(0,z) + i_*Pi;

        .sort:endiPis;
