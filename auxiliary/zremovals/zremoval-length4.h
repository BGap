        bracket G;
        .sort:bracklen4;
        keep brackets;

* 4 zs
	id G('z1','z1','z1','z1') = - G(0,0,0,'z1');

* 3 zs
	id G(a1?!{,'z1'},'z1','z1','z1') =
       + delta(0,a1)*zeta3
       + G(a1,a1,a1,'z1')
       - G(a1,0,a1,'z1')
       - G(0,a1,a1,'z1')
       + G(0,0,a1,'z1')
      ;


* 2 z
        id G(a1?!{,'z1'},'z1',a2?!{,'z1'},'z1') =
       + 2*delta(0,a1)*delta(0,a2)*zeta3
       - delta(0,a1)*G(a2,'z1')*zeta2
       + G(a1,a1,a2,'z1')
       + G(a2,a1,a1,'z1')
       - G(a2,a1,a2,'z1')
       - G(a2,0,a1,'z1')
      ;

        id G(a1?!{,'z1'},a2?!{,'z1'},'z1','z1') =
       - delta(0,a1)*delta(0,a2)*zeta3
       + delta(0,a1)*G(a2,'z1')*zeta2
       - delta(0,a2)*G(a1,'z1')*zeta2
       + G(a1,a2,a2,'z1')
       - G(a1,0,a2,'z1')
       - G(a2,a1,a1,'z1')
       + G(a2,a1,a2,'z1')
       + G(a2,0,a1,'z1')
       - G(0,a1,a2,'z1')
      ;


        id delta(0,0) = 1;
        id once delta(0,1) = 0;
       .sort:len4;
