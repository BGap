        bracket G;
        .sort:beg1cte5;
        keep brackets;

* shuffle ids to remove z1 from the first slot
 	id G('z1','z1','z1','z1',w2?!{,'z1'}) = 1/4*G('z1',w2)*G('z1','z1','z1',w2);

* 3 zs
        id G('z1','z1','z1',a4?!{,'z1'},w2?!{,'z1'}) =
        + 1/3*G('z1',w2)*G('z1','z1',a4,w2)
        - 1/3*G('z1','z1',a4,'z1',w2)
        ;

        id G('z1','z1',a3?!{,'z1'},'z1',w2?!{,'z1'}) =
        + 1/2*G('z1',w2)*G('z1',a3,'z1',w2)
        - G('z1',a3,'z1','z1',w2)
        ;

        id G('z1',a2?!{,'z1'},'z1','z1',w2?!{,'z1'}) =
        + G('z1',w2)*G(a2,'z1','z1',w2)
        - 3*G(a2,'z1','z1','z1',w2);

* 2 zs
        id G('z1','z1',a3?!{,'z1'},a4?!{,'z1'},w2?!{,'z1'}) =
        + 1/2*G('z1',w2)*G('z1',a3,a4,w2)
        - 1/2*G('z1',a3,'z1',a4,w2)
        - 1/2*G('z1',a3,a4,'z1',w2)
        ;

        id G('z1',a3?!{,'z1'},'z1',a4?!{,'z1'},w2?!{,'z1'}) =
        + G('z1',w2)*G(a3,'z1',a4,w2)
        - 2*G(a3,'z1','z1',a4,w2)
        - G(a3,'z1',a4,'z1',w2)
        ;

        id G('z1',a3?!{,'z1'},a4?!{,'z1'},'z1',w2?!{,'z1'}) =
        + G('z1',w2)*G(a3,a4,'z1',w2)
        - G(a3,'z1',a4,'z1',w2)
        - 2*G(a3,a4,'z1','z1',w2)
        ;

* 1 z
        id G('z1',a2?!{,'z1'},a3?!{,'z1'},a4?!{,'z1'},w2?!{,'z1'}) =
        + G('z1',w2)*G(a2,a3,a4,w2)
        - G(a2,'z1',a3,a4,w2)
        - G(a2,a3,'z1',a4,w2)
        - G(a2,a3,a4,'z1',w2)
        ;



   id G(a1?!{,'z1'},'z1','z1','z1',w2?!{,'z1'}) =
       + delta(0,a1)*G(a1,'z1')*zeta3
       - delta(0,a1)*G(0,'z1')*zeta3
       - G(a1,a1,a1,w2,'z1')
       + G(a1,a1,a1,'z1')*G(a1,w2)
       + G(a1,a1,a1,0,'z1')
       - G(a1,a1,w2,w2,'z1')
       + G(a1,a1,w2,0,'z1')
       + G(a1,a1,0,w2,'z1')
       - G(a1,a1,0,'z1')*G(a1,w2)
       - G(a1,a1,0,0,'z1')
       - G(a1,w2,w2,w2,'z1')
       + G(a1,w2,w2,0,'z1')
       + G(a1,w2,0,w2,'z1')
       - G(a1,w2,0,0,'z1')
       + G(a1,0,a1,w2,'z1')
       - G(a1,0,a1,'z1')*G(a1,w2)
       - G(a1,0,a1,0,'z1')
       + G(a1,0,w2)*G(a1,a1,'z1')
       - G(a1,0,w2)*G(a1,0,'z1')
       + G(a1,0,w2,w2,'z1')
       - G(a1,0,w2,0,'z1')
       + G(a1,0,0,w2)*G(a1,'z1')
       - G(a1,0,0,w2,'z1')
       + G(a1,0,0,'z1')*G(a1,w2)
       + G(a1,0,0,0,w2)
       + G(a1,0,0,0,'z1')
       + G(0,a1,a1,w2,'z1')
       - G(0,a1,a1,'z1')*G(a1,w2)
       + G(0,a1,a1,'z1')*G(0,w2)
       - G(0,a1,a1,0,'z1')
       + G(0,a1,w2,w2,'z1')
       + G(0,a1,w2,'z1')*G(0,w2)
       - G(0,a1,w2,0,'z1')
       - G(0,a1,'z1')*G(a1,0,w2)
       - G(0,a1,0,w2,'z1')
       + G(0,a1,0,'z1')*G(a1,w2)
       - G(0,a1,0,'z1')*G(0,w2)
       + G(0,a1,0,0,'z1')
       - G(0,w2)*G(a1,a1,a1,'z1')
       - G(0,w2)*G(a1,a1,w2,'z1')
       + G(0,w2)*G(a1,a1,0,'z1')
       - G(0,w2)*G(a1,w2,w2,'z1')
       + G(0,w2)*G(a1,w2,0,'z1')
       + G(0,w2)*G(a1,0,a1,'z1')
       + G(0,w2)*G(a1,0,w2,'z1')
       - G(0,w2)*G(a1,0,0,'z1')
       - G(0,'z1')*G(a1,0,0,w2)
       - G(0,0,a1,w2,'z1')
       + G(0,0,a1,'z1')*G(a1,w2)
       - G(0,0,a1,'z1')*G(0,w2)
       + G(0,0,a1,0,'z1')
       - G(0,0,w2)*G(a1,a1,'z1')
       - G(0,0,w2)*G(a1,w2,'z1')
       + G(0,0,w2)*G(a1,0,'z1')
       + G(0,0,w2)*G(0,a1,'z1')
       + G(0,0,'z1')*G(a1,0,w2)
       - G(0,0,0,w2)*G(a1,'z1')
        - G(0,0,0,'z1')*G(a1,w2)

* corrections
        - (
        + 11/10*delta(0,a1)*zeta2^2
       + 2*delta(0,a1)*G(a1,a1,w2)*zeta2
       - 2*delta(0,a1)*G(a1,0,w2)*zeta2
       - 2*delta(0,a1)*G(0,a1,w2)*zeta2
       + 2*delta(0,a1)*G(0,0,w2)*zeta2
        )
* erik
        + (
	+ sign(w2,'z1')*G(a1,a1,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a1,'z1')*G(a1,w2)*i_*Pi
       + sign(w2,'z1')*G(a1,a1,w2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a1,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,'z1')*i_*zeta2*Pi
       + sign(w2,'z1')*G(a1,w2)*i_*zeta2*Pi
       + sign(w2,'z1')*G(a1,w2,w2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,0,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,0,'z1')*G(a1,w2)*i_*Pi
       - sign(w2,'z1')*G(a1,0,w2)*G(a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,0,w2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,0,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,0,0,w2)*i_*Pi
       - sign(w2,'z1')*G(0,a1,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,a1,'z1')*G(a1,w2)*i_*Pi
       - sign(w2,'z1')*G(0,a1,'z1')*G(0,w2)*i_*Pi
       - sign(w2,'z1')*G(0,a1,w2,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,a1,0,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,'z1')*G(a1,0,w2)*i_*Pi
       + sign(w2,'z1')*G(0,w2)*G(a1,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,w2)*G(a1,w2,'z1')*i_*Pi
       - sign(w2,'z1')*G(0,w2)*G(a1,0,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,0,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(0,0,'z1')*G(a1,w2)*i_*Pi
       + sign(w2,'z1')*G(0,0,w2)*G(a1,'z1')*i_*Pi
       + 3*G(a1,a1,'z1')*zeta2
       - 3*G(a1,'z1')*G(a1,w2)*zeta2
       + 3*G(a1,w2,'z1')*zeta2
       - 3*G(a1,0,'z1')*zeta2
       - 3*G(a1,0,w2)*zeta2
       - 3*G(0,a1,'z1')*zeta2
       + 3*G(0,'z1')*G(a1,w2)*zeta2
       + 3*G(0,w2)*G(a1,'z1')*zeta2
        )
        + 3*delta(a1,0)*zeta2^2
      ;

	id G(a1?!{,'z1'},a2?!{,'z1'},'z1','z1',w2?!{,'z1'}) =
       + 2*delta(0,a1)*G(a2,a2,'z1')*zeta2
       - delta(0,a1)*G(a2,'z1')*zeta3
       - 2*delta(0,a1)*G(0,a2,'z1')*zeta2
       - 2*delta(0,a2)*G(a1,w2)*G(a2,'z1')*zeta2
       + 2*delta(0,a2)*G(0,'z1')*G(a1,w2)*zeta2
       + G(a1,a2,w2)*G(a2,a2,'z1')
       - G(a1,a2,w2)*G(a2,0,'z1')
       + G(a1,a2,0,w2)*G(a2,'z1')
       + G(a1,a2,0,0,w2)
       - G(a1,w2)*G(a2,a1,a1,'z1')
       + G(a1,w2)*G(a2,a1,0,'z1')
       - G(a1,w2)*G(a2,a2,a1,'z1')
       + G(a1,w2)*G(a2,a2,0,'z1')
       + G(a1,w2)*G(a2,0,a1,'z1')
       - G(a1,w2)*G(a2,0,0,'z1')
       - G(a1,0,w2)*G(a2,a1,'z1')
       - G(a1,0,w2)*G(a2,a2,'z1')
       + G(a1,0,w2)*G(a2,0,'z1')
       - G(a1,0,0,w2)*G(a2,'z1')
       + G(a2,a1,a1,w2,'z1')
       - G(a2,a1,a1,0,'z1')
       + G(a2,a1,w2,w2,'z1')
       - G(a2,a1,w2,0,'z1')
       - G(a2,a1,0,w2,'z1')
       + G(a2,a1,0,0,'z1')
       + G(a2,a2,a1,w2,'z1')
       - G(a2,a2,a1,0,'z1')
       - G(a2,0,a1,w2,'z1')
       + G(a2,0,a1,0,'z1')
       - G(0,a2,a1,w2,'z1')
       + G(0,a2,a1,'z1')*G(a1,w2)
       - G(0,a2,a1,'z1')*G(0,w2)
       + G(0,a2,a1,0,'z1')
       - G(0,a2,'z1')*G(a1,a2,w2)
       + G(0,a2,'z1')*G(a1,0,w2)
       - G(0,a2,0,'z1')*G(a1,w2)
       + G(0,w2)*G(a2,a1,a1,'z1')
       + G(0,w2)*G(a2,a1,w2,'z1')
       - G(0,w2)*G(a2,a1,0,'z1')
       + G(0,w2)*G(a2,a2,a1,'z1')
       - G(0,w2)*G(a2,0,a1,'z1')
       - G(0,'z1')*G(a1,a2,0,w2)
       + G(0,0,w2)*G(a2,a1,'z1')
        + G(0,0,'z1')*G(a1,a2,w2)

* corrections
        - (
	- 4/5*delta(0,a1)*delta(0,a2)*zeta2^2
       - 2*delta(0,a1)*G(a2,a1,w2)*zeta2
       + 2*delta(0,a1)*G(a2,0,w2)*zeta2
       - delta(0,a2)*G(a1,w2)*zeta3
        )
* eriks corrections
      + (
	 - 3*G(a1,a2,w2)*zeta2
       + 3*G(a1,w2)*G(a2,'z1')*zeta2
       - 3*G(a2,a1,'z1')*zeta2
        )
        + (
	- sign(w2,'z1')*G(a1,a2,w2)*G(a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a2,0,w2)*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a2,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a2,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2)*G(a2,0,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,0,w2)*G(a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a1,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a1,w2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,a1,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a2,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,0,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,a2,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(0,a2,'z1')*G(a1,w2)*i_*Pi
       + sign(w2,'z1')*G(0,'z1')*G(a1,a2,w2)*i_*Pi
        - sign(w2,'z1')*G(0,w2)*G(a2,a1,'z1')*i_*Pi

        - sign(w2,'z1')*G(a2,'z1')*i_*zeta2*Pi*delta(a1,0)
        + sign(w2,'z1')*G(a1,w2)*i_*zeta2*Pi*delta(a2,0)
        - sign(w2,'z1')*i_*zeta3*Pi*delta(a1,0)*delta(a2,0)
        )

      ;

        id G(a1?!{,'z1'},'z1',a2?!{,'z1'},'z1',w2?!{,'z1'}) =
       + 2*delta(0,a1)*G(a1,a2,'z1')*zeta2
       - 4*delta(0,a1)*G(a2,a2,'z1')*zeta2
       + 2*delta(0,a1)*G(0,a2,'z1')*zeta2
       - 2*delta(0,a2)*G(a1,w2)*G(a1,'z1')*zeta2
       + 4*delta(0,a2)*G(a1,w2)*G(a2,'z1')*zeta2
       + 2*delta(0,a2)*G(a1,w2,'z1')*zeta2
       + 2*delta(0,a2)*G(a1,'z1')*zeta3
       - 2*delta(0,a2)*G(a1,0,'z1')*zeta2
       - 2*delta(0,a2)*G(a2,a1,'z1')*zeta2
       + 2*delta(0,a2)*G(0,a1,'z1')*zeta2
       + 2*delta(0,a2)*G(0,w2)*G(a1,'z1')*zeta2
       - 2*delta(0,a2)*G(0,'z1')*G(a1,w2)*zeta2
       + G(a1,a2,a1,w2,'z1')
       - G(a1,a2,a1,'z1')*G(a1,w2)
       - G(a1,a2,a1,0,'z1')
       - 2*G(a1,a2,a2,w2,'z1')
       + 2*G(a1,a2,a2,'z1')*G(a2,w2)
       + 2*G(a1,a2,a2,0,'z1')
       + G(a1,a2,w2)*G(a1,a2,'z1')
       + G(a1,a2,w2)*G(a2,a1,'z1')
       - 2*G(a1,a2,w2)*G(a2,a2,'z1')
       + G(a1,a2,w2)*G(a2,0,'z1')
       - G(a1,a2,w2,'z1')*G(a2,w2)
       + G(a1,a2,'z1')*G(a2,0,w2)
       + G(a1,a2,0,w2)*G(a1,'z1')
       - G(a1,a2,0,w2)*G(a2,'z1')
       + G(a1,a2,0,'z1')*G(a1,w2)
       - G(a1,a2,0,'z1')*G(a2,w2)
       + 2*G(a1,w2)*G(a2,a2,a1,'z1')
       - 2*G(a1,w2)*G(a2,a2,0,'z1')
       + G(a1,w2,a2,w2,'z1')
       - G(a1,w2,a2,'z1')*G(a2,w2)
       - G(a1,w2,a2,0,'z1')
       - G(a1,w2,'z1')*G(a2,0,w2)
       + G(a1,w2,0,'z1')*G(a2,w2)
       + G(a1,0,a2,w2)*G(a2,'z1')
       + G(a1,0,a2,w2,'z1')
       - G(a1,0,a2,'z1')*G(a2,w2)
       + G(a1,0,a2,0,w2)
       - G(a1,0,a2,0,'z1')
       - G(a1,0,w2)*G(a1,a2,'z1')
       + 2*G(a1,0,w2)*G(a2,a2,'z1')
       + G(a1,0,w2,'z1')*G(a2,w2)
       - G(a1,0,'z1')*G(a1,a2,w2)
       - G(a2,a1,a2,w2,'z1')
       + G(a2,a1,a2,'z1')*G(a2,w2)
       + G(a2,a1,a2,0,'z1')
       - G(a2,a1,w2,'z1')*G(a2,w2)
       - 2*G(a2,a2,a1,w2,'z1')
       + 2*G(a2,a2,a1,0,'z1')
       + G(0,a1,a2,w2,'z1')
       - G(0,a1,a2,'z1')*G(a2,w2)
       + G(0,a1,a2,'z1')*G(0,w2)
       - G(0,a1,a2,0,'z1')
       + G(0,a1,w2,'z1')*G(a2,w2)
       - G(0,a1,'z1')*G(a1,a2,w2)
       + G(0,a1,'z1')*G(0,a2,w2)
       + G(0,a2,a1,w2,'z1')
       - G(0,a2,a1,'z1')*G(a1,w2)
       + G(0,a2,a1,'z1')*G(0,w2)
       - G(0,a2,a1,0,'z1')
       - G(0,a2,w2)*G(a1,a2,'z1')
       + G(0,a2,w2)*G(a1,0,'z1')
       - G(0,a2,w2)*G(a2,a1,'z1')
       + G(0,a2,'z1')*G(a1,a2,w2)
       - G(0,a2,'z1')*G(a1,0,w2)
       - G(0,a2,0,w2)*G(a1,'z1')
       + G(0,a2,0,'z1')*G(a1,w2)
       + G(0,w2)*G(a1,a2,a1,'z1')
       - 2*G(0,w2)*G(a1,a2,a2,'z1')
       + G(0,w2)*G(a1,w2,a2,'z1')
       + G(0,w2)*G(a1,0,a2,'z1')
       - G(0,w2)*G(a2,a1,a2,'z1')
       - 2*G(0,w2)*G(a2,a2,a1,'z1')
        - G(0,'z1')*G(a1,0,a2,w2)

* corrections
        - (
	- 2/5*delta(0,a1)*delta(0,a2)*zeta2^2
       + 4*delta(0,a2)*G(a1,a2,w2)*zeta2
       + 2*delta(0,a2)*G(a1,w2)*zeta3
       - 2*delta(0,a2)*G(a1,0,w2)*zeta2
        )
* erik
        + (
	- sign(w2,'z1')*G(a1,a2,a1,'z1')*i_*Pi
       + 2*sign(w2,'z1')*G(a1,a2,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,a2,'z1')*G(a1,w2)*i_*Pi
       - sign(w2,'z1')*G(a1,a2,'z1')*G(a2,w2)*i_*Pi
       - sign(w2,'z1')*G(a1,a2,w2)*G(a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,a2,w2)*G(a2,'z1')*i_*Pi
       - 2*sign(w2,'z1')*G(a1,w2)*G(a2,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2,'z1')*G(a2,w2)*i_*Pi
       - sign(w2,'z1')*G(a1,0,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,0,a2,w2)*i_*Pi
       + sign(w2,'z1')*G(a2,a1,a2,'z1')*i_*Pi
       + 2*sign(w2,'z1')*G(a2,a2,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(0,a1,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(0,a2,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,a2,'z1')*G(a1,w2)*i_*Pi
       + sign(w2,'z1')*G(0,a2,w2)*G(a1,'z1')*i_*Pi
        )
        + 2*sign(w2,'z1')*i_*zeta3*Pi*delta(a1,0)*delta(a2,0)
      ;

        id delta(0,0) = 1;
        id once delta(0,1) = 0;
        bracket G;
        .sort:beg2cte5;
        keep brackets;

        id G(a1?!{,'z1'},'z1','z1',a2?!{,'z1'},w2?!{,'z1'}) =
       - 2*delta(0,a1)*G(a1,a2,'z1')*zeta2
       + 2*delta(0,a1)*G(a2,a2,'z1')*zeta2
       + delta(0,a1)*G(a2,'z1')*zeta3
       - 2*delta(0,a2)*G(a1,a1,'z1')*zeta2
       + 2*delta(0,a2)*G(a1,w2)*G(a1,'z1')*zeta2
       - 2*delta(0,a2)*G(a1,w2)*G(a2,'z1')*zeta2
       - 2*delta(0,a2)*G(a1,w2,'z1')*zeta2
       - delta(0,a2)*G(a1,'z1')*zeta3
       + 2*delta(0,a2)*G(a1,0,'z1')*zeta2
       + 2*delta(0,a2)*G(a2,a1,'z1')*zeta2
       - 2*delta(0,a2)*G(0,w2)*G(a1,'z1')*zeta2
       - G(a1,a1,a2,w2,'z1')
       + G(a1,a1,a2,'z1')*G(a2,w2)
       + G(a1,a1,a2,0,'z1')
       - G(a1,a1,w2,'z1')*G(a2,w2)
       + G(a1,a1,'z1')*G(a1,a2,w2)
       - G(a1,a2,a1,w2,'z1')
       + G(a1,a2,a1,'z1')*G(a1,w2)
       + G(a1,a2,a1,0,'z1')
       + G(a1,a2,a2,w2,'z1')
       - G(a1,a2,a2,'z1')*G(a2,w2)
       - G(a1,a2,a2,0,'z1')
       - G(a1,a2,w2)*G(a1,a2,'z1')
       - G(a1,a2,w2)*G(a2,a1,'z1')
       + G(a1,a2,w2)*G(a2,a2,'z1')
       - G(a1,a2,w2,w2,'z1')
       + G(a1,a2,w2,'z1')*G(a2,w2)
       + G(a1,a2,w2,0,'z1')
       + G(a1,a2,0,w2,'z1')
       - G(a1,a2,0,'z1')*G(a1,w2)
       - G(a1,a2,0,0,'z1')
       + G(a1,w2)*G(a2,a1,a1,'z1')
       - G(a1,w2)*G(a2,a1,0,'z1')
       - G(a1,w2)*G(a2,a2,a1,'z1')
       + G(a1,w2)*G(a2,a2,0,'z1')
       - G(a1,w2)*G(a2,0,a1,'z1')
       + G(a1,w2)*G(a2,0,0,'z1')
       - G(a1,w2,a2,w2,'z1')
       + G(a1,w2,a2,'z1')*G(a2,w2)
       + G(a1,w2,a2,0,'z1')
       - G(a1,w2,w2,'z1')*G(a2,w2)
       + G(a1,0,a2,w2)*G(a1,'z1')
       - G(a1,0,a2,w2)*G(a2,'z1')
       + G(a1,0,w2)*G(a1,a2,'z1')
       + G(a1,0,w2)*G(a2,a1,'z1')
       - G(a1,0,w2)*G(a2,a2,'z1')
       - G(a1,0,w2)*G(a2,0,'z1')
       + G(a1,0,0,a2,w2)
       + G(a1,0,0,w2)*G(a2,'z1')
       - G(a2,a1,a1,w2,'z1')
       + G(a2,a1,a1,0,'z1')
       + G(a2,a1,a2,w2,'z1')
       - G(a2,a1,a2,'z1')*G(a2,w2)
       - G(a2,a1,a2,0,'z1')
       - G(a2,a1,w2,w2,'z1')
       + G(a2,a1,w2,'z1')*G(a2,w2)
       + G(a2,a1,w2,0,'z1')
       + G(a2,a1,0,w2,'z1')
       - G(a2,a1,0,0,'z1')
       + G(a2,a2,a1,w2,'z1')
       - G(a2,a2,a1,0,'z1')
       + G(a2,0,a1,w2,'z1')
       - G(a2,0,a1,0,'z1')
       - G(0,a2,w2)*G(a1,a1,'z1')
       + G(0,a2,w2)*G(a1,a2,'z1')
       - G(0,a2,w2)*G(a1,w2,'z1')
       + G(0,a2,w2)*G(a2,a1,'z1')
       - G(0,w2)*G(a1,a1,a2,'z1')
       - G(0,w2)*G(a1,a2,a1,'z1')
       + G(0,w2)*G(a1,a2,a2,'z1')
       - G(0,w2)*G(a1,a2,w2,'z1')
       + G(0,w2)*G(a1,a2,0,'z1')
       - G(0,w2)*G(a1,w2,a2,'z1')
       - G(0,w2)*G(a2,a1,a1,'z1')
       + G(0,w2)*G(a2,a1,a2,'z1')
       - G(0,w2)*G(a2,a1,w2,'z1')
       + G(0,w2)*G(a2,a1,0,'z1')
       + G(0,w2)*G(a2,a2,a1,'z1')
       + G(0,w2)*G(a2,0,a1,'z1')
       - G(0,0,a2,w2)*G(a1,'z1')
       - G(0,0,w2)*G(a1,a2,'z1')
        - G(0,0,w2)*G(a2,a1,'z1')

* corrections
        - (
	+ 2*delta(0,a1)*delta(0,a2)*zeta2^2
       + 2*delta(0,a1)*G(a2,a1,w2)*zeta2
       - 2*delta(0,a1)*G(a2,0,w2)*zeta2
       - 2*delta(0,a2)*G(a1,a2,w2)*zeta2
       - delta(0,a2)*G(a1,w2)*zeta3
        )
* eriks
+ (
   + 3*G(a1,a2,'z1')*zeta2
       - 3*G(a1,w2)*G(a2,'z1')*zeta2
       + 3*G(a2,a1,'z1')*zeta2
        )
* erik again
        + (
	+ sign(w2,'z1')*G(a1,a1,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,a2,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a2,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a2,'z1')*G(a1,w2)*i_*Pi
       + sign(w2,'z1')*G(a1,a2,w2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,a2,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2)*G(a2,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a2,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a2,0,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,0,w2)*G(a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,a1,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a1,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,a1,w2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a1,0,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a2,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,0,a1,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,w2)*G(a1,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(0,w2)*G(a2,a1,'z1')*i_*Pi
        )
* erik again2
        + (
        + sign(w2,'z1')*G(a2,'z1')*i_*zeta2*Pi*delta(a1,0)
        + sign(w2,'z1')*G(a1,'z1')*i_*zeta2*Pi*delta(a2,0)
        - sign(w2,'z1')*G(a1,w2)*i_*zeta2*Pi*delta(a2,0)
        )
      ;

        id G(a1?!{,'z1'},a2?!{,'z1'},a3?!{,'z1'},'z1',w2?!{,'z1'}) =
       - 2*delta(0,a1)*G(a3,a2,'z1')*zeta2
       + 2*delta(0,a2)*G(a1,w2)*G(a3,'z1')*zeta2
       + G(a1,a2,a3,w2)*G(a3,'z1')
       + G(a1,a2,a3,0,w2)
       - G(a1,a2,w2)*G(a3,a2,'z1')
       + G(a1,a2,w2)*G(a3,0,'z1')
       - G(a1,a2,0,w2)*G(a3,'z1')
       + G(a1,w2)*G(a3,a2,a1,'z1')
       - G(a1,w2)*G(a3,a2,0,'z1')
       + G(a1,0,w2)*G(a3,a2,'z1')
       - G(a3,a2,a1,w2,'z1')
       + G(a3,a2,a1,0,'z1')
       - G(0,w2)*G(a3,a2,a1,'z1')
       - G(0,'z1')*G(a1,a2,a3,w2)

* corrections
       - 4/5*delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta2^2
        - 2*delta(0,a3)*G(a1,a2,w2)*zeta2
* erik
        + (
	- sign(w2,'z1')*G(a1,a2,a3,w2)*i_*Pi
       + sign(w2,'z1')*G(a1,a2,w2)*G(a3,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2)*G(a3,a2,'z1')*i_*Pi
       + sign(w2,'z1')*G(a3,a2,a1,'z1')*i_*Pi
        )
      ;

        id G(a1?!{,'z1'},a2?!{,'z1'},'z1',a3?!{,'z1'},w2?!{,'z1'}) =
       + 2*delta(0,a1)*G(a2,a3,'z1')*zeta2
       + 2*delta(0,a1)*G(a3,a2,'z1')*zeta2
       - 2*delta(0,a2)*G(a1,w2)*G(a3,'z1')*zeta2
       - 2*delta(0,a3)*G(a1,w2)*G(a2,'z1')*zeta2
       + 2*delta(0,a3)*G(a2,a1,'z1')*zeta2
       + G(a1,a2,a3,w2)*G(a2,'z1')
       - G(a1,a2,a3,w2)*G(a3,'z1')
       + G(a1,a2,w2)*G(a3,a2,'z1')
       - G(a1,a2,w2)*G(a3,0,'z1')
       + G(a1,a2,0,a3,w2)
       + G(a1,a2,0,w2)*G(a3,'z1')
       - G(a1,a3,w2)*G(a2,a1,'z1')
       + G(a1,a3,w2)*G(a2,a3,'z1')
       - G(a1,w2)*G(a2,a3,a1,'z1')
       + G(a1,w2)*G(a2,a3,0,'z1')
       - G(a1,w2)*G(a3,a2,a1,'z1')
       + G(a1,w2)*G(a3,a2,0,'z1')
       - G(a1,0,a3,w2)*G(a2,'z1')
       - G(a1,0,w2)*G(a2,a3,'z1')
       - G(a1,0,w2)*G(a3,a2,'z1')
       + G(a2,a1,a3,w2,'z1')
       - G(a2,a1,a3,'z1')*G(a3,w2)
       - G(a2,a1,a3,0,'z1')
       + G(a2,a1,w2,'z1')*G(a3,w2)
       + G(a2,a3,a1,w2,'z1')
       - G(a2,a3,a1,0,'z1')
       + G(a3,a2,a1,w2,'z1')
       - G(a3,a2,a1,0,'z1')
       + G(0,a3,w2)*G(a2,a1,'z1')
       + G(0,w2)*G(a2,a1,a3,'z1')
       + G(0,w2)*G(a2,a3,a1,'z1')
        + G(0,w2)*G(a3,a2,a1,'z1')

* corrections
        - (
	- 12/5*delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta2^2
       - 2*delta(0,a3)*G(a1,a2,w2)*zeta2
        )
* erik
        + (
	- sign(w2,'z1')*G(a1,a2,w2)*G(a3,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a2,a3,'z1')*i_*Pi
       + sign(w2,'z1')*G(a1,w2)*G(a3,a2,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a1,a3,'z1')*i_*Pi
       - sign(w2,'z1')*G(a2,a3,a1,'z1')*i_*Pi
       - sign(w2,'z1')*G(a3,a2,a1,'z1')*i_*Pi
        )
      ;

        id G(a1?!{,'z1'},'z1',a2?!{,'z1'},a3?!{,'z1'},w2?!{,'z1'}) =
       - 2*delta(0,a1)*G(a2,a3,'z1')*zeta2
       - 2*delta(0,a3)*G(a1,a2,'z1')*zeta2
       + 2*delta(0,a3)*G(a1,w2)*G(a2,'z1')*zeta2
       - 2*delta(0,a3)*G(a2,a1,'z1')*zeta2
       + G(a1,a2,a3,w2)*G(a1,'z1')
       - G(a1,a2,a3,w2)*G(a2,'z1')
       - G(a1,a2,a3,w2,'z1')
       + G(a1,a2,a3,'z1')*G(a3,w2)
       + G(a1,a2,a3,0,'z1')
       - G(a1,a2,w2,'z1')*G(a3,w2)
       + G(a1,a2,'z1')*G(a2,a3,w2)
       + G(a1,a3,w2)*G(a2,a1,'z1')
       - G(a1,a3,w2)*G(a2,a3,'z1')
       + G(a1,w2)*G(a2,a3,a1,'z1')
       - G(a1,w2)*G(a2,a3,0,'z1')
       - G(a1,w2,'z1')*G(a2,a3,w2)
       + G(a1,0,a2,a3,w2)
       + G(a1,0,a3,w2)*G(a2,'z1')
       + G(a1,0,w2)*G(a2,a3,'z1')
       - G(a2,a1,a3,w2,'z1')
       + G(a2,a1,a3,'z1')*G(a3,w2)
       + G(a2,a1,a3,0,'z1')
       - G(a2,a1,w2,'z1')*G(a3,w2)
       - G(a2,a3,a1,w2,'z1')
       + G(a2,a3,a1,0,'z1')
       - G(0,a2,a3,w2)*G(a1,'z1')
       - G(0,a3,w2)*G(a1,a2,'z1')
       - G(0,a3,w2)*G(a2,a1,'z1')
       - G(0,w2)*G(a1,a2,a3,'z1')
       - G(0,w2)*G(a2,a1,a3,'z1')
        - G(0,w2)*G(a2,a3,a1,'z1')

* corrections
        - 12/5*delta(0,a1)*delta(0,a2)*delta(0,a3)*zeta2^2
* erik
        + (
	+ sign(w2,'z1')*G(a1,a2,a3,'z1')*i_*Pi
       - sign(w2,'z1')*G(a1,w2)*G(a2,a3,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,a1,a3,'z1')*i_*Pi
       + sign(w2,'z1')*G(a2,a3,a1,'z1')*i_*Pi
        )
      ;

        id delta(0,0) = 1;
        id delta(0,1) = 0;
        #call ContourSign()
        .sort:Ctelen5;
