
        bracket G;
        .sort:beg1cte4;
        keep brackets;

* shuffle ids to eliminate z1 from the first slot 
   	id G('z1','z1','z1',w2?!{,'z1'}) = 1/3*G('z1',w2)*G('z1','z1',w2);

	id G('z1','z1',a3?!{,'z1'},w2?!{,'z1'}) =
        + 1/2*G('z1',w2)*G('z1',a3,w2)
        - 1/2*G('z1',a3,'z1',w2)
        ;

	id G('z1',a2?!{,'z1'},'z1',w2?!{,'z1'}) =
        + G('z1',w2)*G(a2,'z1',w2)
        - 2*G(a2,'z1','z1',w2)
        ;

        id G('z1',a2?!{,'z1'},a3?!{,'z1'},w2?!{,'z1'}) =
        + G('z1',w2)*G(a2,a3,w2)
        - G(a2,'z1',a3,w2)
        - G(a2,a3,'z1',w2)
        ;

* end shuffles

   id G(a1?!{,'z1'},a2?!{,'z1'},'z1',w2?!{,'z1'}) =
       + G(a1,a2,w2)*G(a2,'z1')
       + G(a1,a2,0,w2)
       - G(a1,w2)*G(a2,a1,'z1')
       + G(a1,w2)*G(a2,0,'z1')
       - G(a1,0,w2)*G(a2,'z1')
       + G(a2,a1,w2,'z1')
       - G(a2,a1,0,'z1')
       + 2*G(a2,'z1')*zeta2*delta(a1,0)
       + G(0,w2)*G(a2,a1,'z1')
       - G(0,'z1')*G(a1,a2,w2)
       - 2*G(a1,w2)*zeta2*delta(a2,0)
* erik
	+ (
        - Pi*sign(w2,'z1')*G(a2,a1,'z1')*i_
        - Pi*sign(w2,'z1')*G(a1,a2,w2)*i_
	+ Pi*sign(w2,'z1')*G(a2,'z1')*G(a1,w2)*i_
	)
       ;

        id G(a1?!{,'z1'},'z1',a2?!{,'z1'},w2?!{,'z1'}) =
       + G(a1,a2,w2)*G(a1,'z1')
       - G(a1,a2,w2)*G(a2,'z1')
       - G(a1,a2,w2,'z1')
       + G(a1,a2,'z1')*G(a2,w2)
       + G(a1,a2,0,'z1')
       + G(a1,w2)*G(a2,a1,'z1')
       - G(a1,w2)*G(a2,0,'z1')
       - G(a1,w2,'z1')*G(a2,w2)
       - 2*G(a1,'z1')*zeta2*delta(a2,0)
       + G(a1,0,a2,w2)
       + G(a1,0,w2)*G(a2,'z1')
       - G(a2,a1,w2,'z1')
       + G(a2,a1,0,'z1')
       - 2*G(a2,'z1')*zeta2*delta(a1,0)
       - G(0,a2,w2)*G(a1,'z1')
       - G(0,w2)*G(a1,a2,'z1')
       - G(0,w2)*G(a2,a1,'z1')
        + 2*G(a1,w2)*zeta2*delta(a2,0)

* erik
        + (
        + Pi*sign(w2,'z1')*G(a1,a2,'z1')*i_
        - Pi*sign(w2,'z1')*G(a2,'z1')*G(a1,w2)*i_
        + Pi*sign(w2,'z1')*G(a2,a1,'z1')*i_
        )
      ;

        id G(a1?!{,'z1'},'z1','z1',w2?!{,'z1'}) =
       - G(a1,a1,w2,'z1')
       + G(a1,a1,'z1')*G(a1,w2)
       + G(a1,a1,0,'z1')
       - G(a1,w2,w2,'z1')
       + G(a1,w2,0,'z1')
       + G(a1,0,w2)*G(a1,'z1')
       + G(a1,0,w2,'z1')
       - G(a1,0,'z1')*G(a1,w2)
       + G(a1,0,0,w2)
       - G(a1,0,0,'z1')
       + G(0,a1,w2,'z1')
       - G(0,a1,'z1')*G(a1,w2)
       + G(0,a1,'z1')*G(0,w2)
       - G(0,a1,0,'z1')
       - G(0,w2)*G(a1,a1,'z1')
       - G(0,w2)*G(a1,w2,'z1')
       + G(0,w2)*G(a1,0,'z1')
       - G(0,'z1')*G(a1,0,w2)
       - G(0,0,w2)*G(a1,'z1')
        + G(0,0,'z1')*G(a1,w2)
        + zeta3*delta(a1,0)
* erik
        + (
        + Pi*sign(w2,'z1')*G(a1,'z1')*G(0,w2)*i_
        + Pi*sign(w2,'z1')*G(0,'z1')*G(a1,w2)*i_
        - Pi*sign(w2,'z1')*G(a1,'z1')*G(a1,w2)*i_
        - Pi*sign(w2,'z1')*G(0,a1,'z1')*i_
        + Pi*sign(w2,'z1')*G(a1,w2,'z1')*i_
        - Pi*sign(w2,'z1')*G(a1,0,'z1')*i_
        + Pi*sign(w2,'z1')*G(a1,a1,'z1')*i_
        - Pi*sign(w2,'z1')*G(a1,0,w2)*i_
        - 1/2*Pi^2*sign(w2,'z1')^2*G(a1,w2)
        + 1/2*Pi^2*sign(w2,'z1')^2*G(a1,'z1')
* erik again
        + Pi*sign(w2,'z1')*zeta2*i_*delta(a1,0)
        )
      ;


        id Pi^2 = 6*zeta2;
        id sign(i?,j?)^2 = 1;
        id delta(0,0) = 1;
        id once delta(0,1) = 0;
        #call ContourSign()
        .sort:Ctelen4;
