        bracket G;
        .sort
        keep brackets;

* the first two require eliminating the z1 from the RHS using lower-order ids
        id G('z1','z1',w2?!{,'z1'}) =
        + 1/2*G('z1',w2)*G('z1',w2)
	;

        id G('z1',a2?!{,'z1'},w2?!{,'z1'}) =
        + G('z1',w2)*G(a2,w2)
        - G(a2,'z1',w2)
        ;

        id G(a1?!{,'z1'},'z1',w2?!{,'z1'}) =
        + G(a1,'z1')*G(a1,w2)
        - G(a1,w2,'z1')
        + G(a1,0,'z1')
        + G(a1,0,w2)
        - G(0,'z1')*G(a1,w2)
        - G(0,w2)*G(a1,'z1')
        - 2*zeta2*delta(a1,0)
* erik
        - i_*Pi*sign(w2,'z1')*G(a1,w2)
        + i_*Pi*sign(w2,'z1')*G(a1,'z1')

        ;

*        .sort:Ctelen3;
