* computed in convertHPL.frm




      id HPL(0,0,0,0,0,0,0,1)= (
         -24/175*zeta2^4
         );



      id HPL(0,0,0,0,0,0,1,0)= (
         +24/25*zeta2^4
         );



      id HPL(0,0,0,0,0,0,1,1)= (
         +6/35*zeta2^4
         -zeta3*zeta5
         );



      id HPL(0,0,0,0,0,1,0,0)= (
         -72/25*zeta2^4
         );



      id HPL(0,0,0,0,0,1,0,1)= (
         -42/125*zeta2^4
         +2*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,0,0,0,0,1,1,0)= (
         -606/875*zeta2^4
         +4*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(0,0,0,0,0,1,1,1)= (
         -61/175*zeta2^4
         -1/2*zeta2*zeta3^2
         +3*zeta3*zeta5
         );



      id HPL(0,0,0,0,1,0,0,0)= (
         +24/5*zeta2^4
         );



      id HPL(0,0,0,0,1,0,0,1)= (
         +[zeta5,3]
         );



      id HPL(0,0,0,0,1,0,1,0)= (
         +42/25*zeta2^4
         -10*zeta3*zeta5
         );



      id HPL(0,0,0,0,1,0,1,1)= (
         +2266/2625*zeta2^4
         +zeta2*zeta3^2
         -7*zeta3*zeta5
         +7/10*[zeta5,3]
         );



      id HPL(0,0,0,0,1,1,0,0)= (
         +156/175*zeta2^4
         -5*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,0,0,0,1,1,0,1)= (
         -157/2625*zeta2^4
         +3/2*zeta2*zeta3^2
         -5/2*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,0,0,0,1,1,1,0)= (
         +822/875*zeta2^4
         -11/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(0,0,0,0,1,1,1,1)= (
         +499/1400*zeta2^4
         +zeta2*zeta3^2
         -4*zeta3*zeta5
         );



      id HPL(0,0,0,1,0,0,0,0)= (
         -24/5*zeta2^4
         );



      id HPL(0,0,0,1,0,0,0,1)= (
         +2/175*zeta2^4
         );



      id HPL(0,0,0,1,0,0,1,0)= (
         -6/175*zeta2^4
         -4*[zeta5,3]
         );



      id HPL(0,0,0,1,0,0,1,1)= (
         -29/175*zeta2^4
         -1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         -5/2*[zeta5,3]
         );



      id HPL(0,0,0,1,0,1,0,0)= (
         -582/175*zeta2^4
         +20*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(0,0,0,1,0,1,0,1)= (
         -2386/2625*zeta2^4
         -3*zeta2*zeta3^2
         +11*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,0,0,1,0,1,1,0)= (
         -1936/875*zeta2^4
         +13*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(0,0,0,1,0,1,1,1)= (
         -14899/21000*zeta2^4
         -2*zeta2*zeta3^2
         +8*zeta3*zeta5
         -3/5*[zeta5,3]
         );



      id HPL(0,0,0,1,1,0,0,0)= (
         -2/25*zeta2^4
         );



      id HPL(0,0,0,1,1,0,0,1)= (
         -142/525*zeta2^4
         -1/2*zeta2*zeta3^2
         +5/2*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(0,0,0,1,1,0,1,0)= (
         +1478/875*zeta2^4
         -2*zeta2*zeta3^2
         -6*zeta3*zeta5
         -11/5*[zeta5,3]
         );



      id HPL(0,0,0,1,1,0,1,1)= (
         -1919/4200*zeta2^4
         -3/2*zeta2*zeta3^2
         +11/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(0,0,0,1,1,1,0,0)= (
         -283/175*zeta2^4
         +zeta2*zeta3^2
         +15/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(0,0,0,1,1,1,0,1)= (
         +7457/21000*zeta2^4
         +1/2*zeta2*zeta3^2
         -3*zeta3*zeta5
         -6/5*[zeta5,3]
         );



      id HPL(0,0,0,1,1,1,1,0)= (
         -4301/7000*zeta2^4
         -zeta2*zeta3^2
         +11/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(0,0,0,1,1,1,1,1)= (
         -61/175*zeta2^4
         -1/2*zeta2*zeta3^2
         +3*zeta3*zeta5
         );



      id HPL(0,0,1,0,0,0,0,0)= (
         +72/25*zeta2^4
         );



      id HPL(0,0,1,0,0,0,0,1)= (
         -24/175*zeta2^4
         +zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,0,1,0,0,0,1,0)= (
         +18/35*zeta2^4
         -4*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(0,0,1,0,0,0,1,1)= (
         +13/21*zeta2^4
         +2*zeta2*zeta3^2
         -15/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(0,0,1,0,0,1,0,0)= (
         -18/25*zeta2^4
         +6*zeta3*zeta5
         );



      id HPL(0,0,1,0,0,1,0,1)= (
         -1187/875*zeta2^4
         -9/2*zeta2*zeta3^2
         +33/2*zeta3*zeta5
         +13/10*[zeta5,3]
         );



      id HPL(0,0,1,0,0,1,1,0)= (
         -3/875*zeta2^4
         +17/10*[zeta5,3]
         );



      id HPL(0,0,1,0,0,1,1,1)= (
         -1919/4200*zeta2^4
         -3/2*zeta2*zeta3^2
         +11/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(0,0,1,0,1,0,0,0)= (
         +666/175*zeta2^4
         -24*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,0,1,0,1,0,0,1)= (
         +56/25*zeta2^4
         +6*zeta2*zeta3^2
         -49/2*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,0,1,0,1,0,1,0)= (
         +24/25*zeta2^4
         +6*zeta2*zeta3^2
         -17*zeta3*zeta5
         );



      id HPL(0,0,1,0,1,0,1,1)= (
         +19007/7000*zeta2^4
         +9*zeta2*zeta3^2
         -33*zeta3*zeta5
         -18/5*[zeta5,3]
         );



      id HPL(0,0,1,0,1,1,0,0)= (
         +2487/875*zeta2^4
         -3*zeta2*zeta3^2
         -11*zeta3*zeta5
         -23/10*[zeta5,3]
         );



      id HPL(0,0,1,0,1,1,0,1)= (
         -28087/21000*zeta2^4
         -3*zeta2*zeta3^2
         +27/2*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,0,1,0,1,1,1,0)= (
         +11651/7000*zeta2^4
         +3*zeta2*zeta3^2
         -31/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(0,0,1,0,1,1,1,1)= (
         +2266/2625*zeta2^4
         +zeta2*zeta3^2
         -7*zeta3*zeta5
         +7/10*[zeta5,3]
         );



      id HPL(0,0,1,1,0,0,0,0)= (
         -156/175*zeta2^4
         +6*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(0,0,1,1,0,0,0,1)= (
         -241/525*zeta2^4
         -3/2*zeta2*zeta3^2
         +11/2*zeta3*zeta5
         );



      id HPL(0,0,1,1,0,0,1,0)= (
         -9/175*zeta2^4
         +1/2*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(0,0,1,1,0,0,1,1)= (
         +1/1400*zeta2^4
         );



      id HPL(0,0,1,1,0,1,0,0)= (
         -2592/875*zeta2^4
         +17*zeta3*zeta5
         +23/10*[zeta5,3]
         );



      id HPL(0,0,1,1,0,1,0,1)= (
         -9491/7000*zeta2^4
         -9/2*zeta2*zeta3^2
         +33/2*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,0,1,1,0,1,1,0)= (
         +69/7000*zeta2^4
         -27/10*[zeta5,3]
         );



      id HPL(0,0,1,1,0,1,1,1)= (
         -29/175*zeta2^4
         -1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         -5/2*[zeta5,3]
         );



      id HPL(0,0,1,1,1,0,0,0)= (
         +58/35*zeta2^4
         -19/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(0,0,1,1,1,0,0,1)= (
         +3043/4200*zeta2^4
         +2*zeta2*zeta3^2
         -8*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(0,0,1,1,1,0,1,0)= (
         +179/1000*zeta2^4
         +2*zeta2*zeta3^2
         -5*zeta3*zeta5
         +21/10*[zeta5,3]
         );



      id HPL(0,0,1,1,1,0,1,1)= (
         +13/21*zeta2^4
         +2*zeta2*zeta3^2
         -15/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(0,0,1,1,1,1,0,0)= (
         -1/200*zeta2^4
         -zeta2*zeta3^2
         +2*zeta3*zeta5
         );



      id HPL(0,0,1,1,1,1,0,1)= (
         -703/875*zeta2^4
         -2*zeta2*zeta3^2
         +17/2*zeta3*zeta5
         +7/10*[zeta5,3]
         );



      id HPL(0,0,1,1,1,1,1,0)= (
         +466/875*zeta2^4
         +zeta2*zeta3^2
         -5*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,0,1,1,1,1,1,1)= (
         +6/35*zeta2^4
         -zeta3*zeta5
         );



      id HPL(0,1,0,0,0,0,0,0)= (
         -24/25*zeta2^4
         );



      id HPL(0,1,0,0,0,0,0,1)= (
         +374/875*zeta2^4
         -2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(0,1,0,0,0,0,1,0)= (
         -326/175*zeta2^4
         +8*zeta3*zeta5
         );



      id HPL(0,1,0,0,0,0,1,1)= (
         -703/875*zeta2^4
         -2*zeta2*zeta3^2
         +17/2*zeta3*zeta5
         +7/10*[zeta5,3]
         );



      id HPL(0,1,0,0,0,1,0,0)= (
         +562/175*zeta2^4
         -12*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,1,0,0,0,1,0,1)= (
         +944/525*zeta2^4
         +5*zeta2*zeta3^2
         -20*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,1,0,0,0,1,1,0)= (
         +466/2625*zeta2^4
         -zeta2*zeta3^2
         +zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(0,1,0,0,0,1,1,1)= (
         +7457/21000*zeta2^4
         +1/2*zeta2*zeta3^2
         -3*zeta3*zeta5
         -6/5*[zeta5,3]
         );



      id HPL(0,1,0,0,1,0,0,0)= (
         -478/175*zeta2^4
         +8*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(0,1,0,0,1,0,0,1)= (
         -793/875*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,0,0,1,0,1,0)= (
         -152/175*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(0,1,0,0,1,0,1,1)= (
         -28087/21000*zeta2^4
         -3*zeta2*zeta3^2
         +27/2*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,0,0,1,1,0,0)= (
         +6/35*zeta2^4
         +5/2*zeta2*zeta3^2
         -6*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,1,0,0,1,1,0,1)= (
         +56249/21000*zeta2^4
         +6*zeta2*zeta3^2
         -27*zeta3*zeta5
         -27/5*[zeta5,3]
         );



      id HPL(0,1,0,0,1,1,1,0)= (
         -31343/21000*zeta2^4
         -3/2*zeta2*zeta3^2
         +23/2*zeta3*zeta5
         +33/10*[zeta5,3]
         );



      id HPL(0,1,0,0,1,1,1,1)= (
         -157/2625*zeta2^4
         +3/2*zeta2*zeta3^2
         -5/2*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,1,0,1,0,0,0,0)= (
         -94/175*zeta2^4
         +8*zeta3*zeta5
         );



      id HPL(0,1,0,1,0,0,0,1)= (
         -803/875*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +11/5*[zeta5,3]
         );



      id HPL(0,1,0,1,0,0,1,0)= (
         +3/35*zeta2^4
         -2*zeta2*zeta3^2
         +4*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,1,0,1,0,0,1,1)= (
         -9491/7000*zeta2^4
         -9/2*zeta2*zeta3^2
         +33/2*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,1,0,1,0,1,0,0)= (
         -31/175*zeta2^4
         -2*zeta2*zeta3^2
         +4*zeta3*zeta5
         );



      id HPL(0,1,0,1,0,1,0,1)= (
         +1/280*zeta2^4
         );



      id HPL(0,1,0,1,0,1,1,0)= (
         -997/21000*zeta2^4
         -3*zeta2*zeta3^2
         +6*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(0,1,0,1,0,1,1,1)= (
         -2386/2625*zeta2^4
         -3*zeta2*zeta3^2
         +11*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,1,0,1,1,0,0,0)= (
         -5119/2625*zeta2^4
         +zeta2*zeta3^2
         +10*zeta3*zeta5
         +11/5*[zeta5,3]
         );



      id HPL(0,1,0,1,1,0,0,1)= (
         -6159/7000*zeta2^4
         -3/2*zeta2*zeta3^2
         +8*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,0,1,1,0,1,0)= (
         -3889/4200*zeta2^4
         -3*zeta2*zeta3^2
         +11*zeta3*zeta5
         );



      id HPL(0,1,0,1,1,0,1,1)= (
         -1187/875*zeta2^4
         -9/2*zeta2*zeta3^2
         +33/2*zeta3*zeta5
         +13/10*[zeta5,3]
         );



      id HPL(0,1,0,1,1,1,0,0)= (
         +6611/21000*zeta2^4
         +3/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -21/10*[zeta5,3]
         );



      id HPL(0,1,0,1,1,1,0,1)= (
         +944/525*zeta2^4
         +5*zeta2*zeta3^2
         -20*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,1,0,1,1,1,1,0)= (
         -997/875*zeta2^4
         -5/2*zeta2*zeta3^2
         +23/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(0,1,0,1,1,1,1,1)= (
         -42/125*zeta2^4
         +2*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,1,1,0,0,0,0,0)= (
         +58/125*zeta2^4
         -4*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,1,1,0,0,0,0,1)= (
         +283/525*zeta2^4
         +zeta2*zeta3^2
         -11/2*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,1,1,0,0,0,1,0)= (
         -841/2625*zeta2^4
         +zeta2*zeta3^2
         +2*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,1,1,0,0,0,1,1)= (
         +3043/4200*zeta2^4
         +2*zeta2*zeta3^2
         -8*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(0,1,1,0,0,1,0,0)= (
         +428/875*zeta2^4
         -1/2*zeta2*zeta3^2
         -11/2*zeta3*zeta5
         -17/10*[zeta5,3]
         );



      id HPL(0,1,1,0,0,1,0,1)= (
         -6159/7000*zeta2^4
         -3/2*zeta2*zeta3^2
         +8*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,1,0,0,1,1,0)= (
         +17/280*zeta2^4
         -1/2*zeta3*zeta5
         );



      id HPL(0,1,1,0,0,1,1,1)= (
         -142/525*zeta2^4
         -1/2*zeta2*zeta3^2
         +5/2*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(0,1,1,0,1,0,0,0)= (
         +4483/2625*zeta2^4
         +zeta2*zeta3^2
         -9*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(0,1,1,0,1,0,0,1)= (
         +12713/7000*zeta2^4
         +4*zeta2*zeta3^2
         -18*zeta3*zeta5
         -27/5*[zeta5,3]
         );



      id HPL(0,1,1,0,1,0,1,0)= (
         +5849/7000*zeta2^4
         +4*zeta2*zeta3^2
         -13*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(0,1,1,0,1,0,1,1)= (
         +56/25*zeta2^4
         +6*zeta2*zeta3^2
         -49/2*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(0,1,1,0,1,1,0,0)= (
         -9757/21000*zeta2^4
         -1/2*zeta2*zeta3^2
         +4*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,1,0,1,1,0,1)= (
         -793/875*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(0,1,1,0,1,1,1,0)= (
         +47/105*zeta2^4
         +zeta2*zeta3^2
         -9/2*zeta3*zeta5
         +5/2*[zeta5,3]
         );



      id HPL(0,1,1,0,1,1,1,1)= (
         +[zeta5,3]
         );



      id HPL(0,1,1,1,0,0,0,0)= (
         -96/125*zeta2^4
         -1/2*zeta2*zeta3^2
         +9/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(0,1,1,1,0,0,0,1)= (
         -3457/4200*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +3*[zeta5,3]
         );



      id HPL(0,1,1,1,0,0,1,0)= (
         +1763/21000*zeta2^4
         -1/2*zeta2*zeta3^2
         -zeta3*zeta5
         -33/10*[zeta5,3]
         );



      id HPL(0,1,1,1,0,0,1,1)= (
         -241/525*zeta2^4
         -3/2*zeta2*zeta3^2
         +11/2*zeta3*zeta5
         );



      id HPL(0,1,1,1,0,1,0,0)= (
         -4573/21000*zeta2^4
         -2*zeta2*zeta3^2
         +7*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(0,1,1,1,0,1,0,1)= (
         -803/875*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +11/5*[zeta5,3]
         );



      id HPL(0,1,1,1,0,1,1,0)= (
         -2/7*zeta2^4
         -1/2*zeta2*zeta3^2
         +3*zeta3*zeta5
         -5/2*[zeta5,3]
         );



      id HPL(0,1,1,1,0,1,1,1)= (
         +2/175*zeta2^4
         );



      id HPL(0,1,1,1,1,0,0,0)= (
         +881/7000*zeta2^4
         +zeta2*zeta3^2
         -7/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(0,1,1,1,1,0,0,1)= (
         +283/525*zeta2^4
         +zeta2*zeta3^2
         -11/2*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,1,1,1,1,0,1,0)= (
         +208/375*zeta2^4
         +zeta2*zeta3^2
         -4*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(0,1,1,1,1,0,1,1)= (
         -24/175*zeta2^4
         +zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(0,1,1,1,1,1,0,0)= (
         -281/875*zeta2^4
         -1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(0,1,1,1,1,1,0,1)= (
         +374/875*zeta2^4
         -2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(0,1,1,1,1,1,1,0)= (
         -54/175*zeta2^4
         +zeta3*zeta5
         );



      id HPL(0,1,1,1,1,1,1,1)= (
         -24/175*zeta2^4
         );



      id HPL(1,0,0,0,0,0,0,0)= (
         +24/175*zeta2^4
         );



      id HPL(1,0,0,0,0,0,0,1)= (
         -54/175*zeta2^4
         +zeta3*zeta5
         );



      id HPL(1,0,0,0,0,0,1,0)= (
         +178/125*zeta2^4
         -4*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(1,0,0,0,0,0,1,1)= (
         +466/875*zeta2^4
         +zeta2*zeta3^2
         -5*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(1,0,0,0,0,1,0,0)= (
         -92/35*zeta2^4
         +6*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,0,0,0,0,1,0,1)= (
         -997/875*zeta2^4
         -5/2*zeta2*zeta3^2
         +23/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(1,0,0,0,0,1,1,0)= (
         -18/25*zeta2^4
         -1/2*zeta2*zeta3^2
         +5*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,0,0,0,0,1,1,1)= (
         -4301/7000*zeta2^4
         -zeta2*zeta3^2
         +11/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(1,0,0,0,1,0,0,0)= (
         +426/175*zeta2^4
         -4*zeta3*zeta5
         );



      id HPL(1,0,0,0,1,0,0,1)= (
         +47/105*zeta2^4
         +zeta2*zeta3^2
         -9/2*zeta3*zeta5
         +5/2*[zeta5,3]
         );



      id HPL(1,0,0,0,1,0,1,0)= (
         +4894/2625*zeta2^4
         +3*zeta2*zeta3^2
         -17*zeta3*zeta5
         -11/5*[zeta5,3]
         );



      id HPL(1,0,0,0,1,0,1,1)= (
         +11651/7000*zeta2^4
         +3*zeta2*zeta3^2
         -31/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(1,0,0,0,1,1,0,0)= (
         +44/105*zeta2^4
         -2*zeta3*zeta5
         );



      id HPL(1,0,0,0,1,1,0,1)= (
         -31343/21000*zeta2^4
         -3/2*zeta2*zeta3^2
         +23/2*zeta3*zeta5
         +33/10*[zeta5,3]
         );



      id HPL(1,0,0,0,1,1,1,0)= (
         +2703/1400*zeta2^4
         +2*zeta2*zeta3^2
         -15*zeta3*zeta5
         -3*[zeta5,3]
         );



      id HPL(1,0,0,0,1,1,1,1)= (
         +822/875*zeta2^4
         -11/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(1,0,0,1,0,0,0,0)= (
         -8/7*zeta2^4
         +zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,0,0,1,0,0,0,1)= (
         -2/7*zeta2^4
         -1/2*zeta2*zeta3^2
         +3*zeta3*zeta5
         -5/2*[zeta5,3]
         );



      id HPL(1,0,0,1,0,0,1,0)= (
         +368/875*zeta2^4
         +1/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,0,0,1,0,0,1,1)= (
         +69/7000*zeta2^4
         -27/10*[zeta5,3]
         );



      id HPL(1,0,0,1,0,1,0,0)= (
         -487/175*zeta2^4
         -4*zeta2*zeta3^2
         +51/2*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,0,0,1,0,1,0,1)= (
         -997/21000*zeta2^4
         -3*zeta2*zeta3^2
         +6*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,0,0,1,0,1,1,0)= (
         -76189/21000*zeta2^4
         -3*zeta2*zeta3^2
         +27*zeta3*zeta5
         +27/5*[zeta5,3]
         );



      id HPL(1,0,0,1,0,1,1,1)= (
         -1936/875*zeta2^4
         +13*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,0,0,1,1,0,0,0)= (
         +79/175*zeta2^4
         +1/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,0,0,1,1,0,0,1)= (
         +17/280*zeta2^4
         -1/2*zeta3*zeta5
         );



      id HPL(1,0,0,1,1,0,1,0)= (
         +36227/21000*zeta2^4
         +3/2*zeta2*zeta3^2
         -25/2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,0,0,1,1,0,1,1)= (
         -3/875*zeta2^4
         +17/10*[zeta5,3]
         );



      id HPL(1,0,0,1,1,1,0,0)= (
         -719/600*zeta2^4
         -3/2*zeta2*zeta3^2
         +19/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(1,0,0,1,1,1,0,1)= (
         +466/2625*zeta2^4
         -zeta2*zeta3^2
         +zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,0,0,1,1,1,1,0)= (
         -18/25*zeta2^4
         -1/2*zeta2*zeta3^2
         +5*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,0,0,1,1,1,1,1)= (
         -606/875*zeta2^4
         +4*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,0,1,0,0,0,0,0)= (
         +494/875*zeta2^4
         -2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,0,1,0,0,0,0,1)= (
         +208/375*zeta2^4
         +zeta2*zeta3^2
         -4*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(1,0,1,0,0,0,1,0)= (
         -383/525*zeta2^4
         -zeta2*zeta3^2
         +zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,0,1,0,0,0,1,1)= (
         +179/1000*zeta2^4
         +2*zeta2*zeta3^2
         -5*zeta3*zeta5
         +21/10*[zeta5,3]
         );



      id HPL(1,0,1,0,0,1,0,0)= (
         +552/875*zeta2^4
         +2*zeta2*zeta3^2
         +zeta3*zeta5
         -13/10*[zeta5,3]
         );



      id HPL(1,0,1,0,0,1,0,1)= (
         -3889/4200*zeta2^4
         -3*zeta2*zeta3^2
         +11*zeta3*zeta5
         );



      id HPL(1,0,1,0,0,1,1,0)= (
         +36227/21000*zeta2^4
         +3/2*zeta2*zeta3^2
         -25/2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,0,1,0,0,1,1,1)= (
         +1478/875*zeta2^4
         -2*zeta2*zeta3^2
         -6*zeta3*zeta5
         -11/5*[zeta5,3]
         );



      id HPL(1,0,1,0,1,0,0,0)= (
         +1307/875*zeta2^4
         +2*zeta2*zeta3^2
         -19*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,0,1,0,1,0,0,1)= (
         +5849/7000*zeta2^4
         +4*zeta2*zeta3^2
         -13*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(1,0,1,0,1,0,1,0)= (
         +381/1400*zeta2^4
         +4*zeta2*zeta3^2
         -8*zeta3*zeta5
         );



      id HPL(1,0,1,0,1,0,1,1)= (
         +24/25*zeta2^4
         +6*zeta2*zeta3^2
         -17*zeta3*zeta5
         );



      id HPL(1,0,1,0,1,1,0,0)= (
         +37603/21000*zeta2^4
         +zeta2*zeta3^2
         -27/2*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,0,1,0,1,1,0,1)= (
         -152/175*zeta2^4
         -2*zeta2*zeta3^2
         +9*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,0,1,0,1,1,1,0)= (
         +4894/2625*zeta2^4
         +3*zeta2*zeta3^2
         -17*zeta3*zeta5
         -11/5*[zeta5,3]
         );



      id HPL(1,0,1,0,1,1,1,1)= (
         +42/25*zeta2^4
         -10*zeta3*zeta5
         );



      id HPL(1,0,1,1,0,0,0,0)= (
         -293/2625*zeta2^4
         -zeta2*zeta3^2
         +9/2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,0,1,1,0,0,0,1)= (
         +1763/21000*zeta2^4
         -1/2*zeta2*zeta3^2
         -zeta3*zeta5
         -33/10*[zeta5,3]
         );



      id HPL(1,0,1,1,0,0,1,0)= (
         -329/1000*zeta2^4
         -zeta2*zeta3^2
         +9*zeta3*zeta5
         +27/5*[zeta5,3]
         );



      id HPL(1,0,1,1,0,0,1,1)= (
         -9/175*zeta2^4
         +1/2*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,0,1,1,0,1,0,0)= (
         -22453/21000*zeta2^4
         -zeta2*zeta3^2
         +2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,0,1,1,0,1,0,1)= (
         +3/35*zeta2^4
         -2*zeta2*zeta3^2
         +4*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(1,0,1,1,0,1,1,0)= (
         +368/875*zeta2^4
         +1/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,0,1,1,0,1,1,1)= (
         -6/175*zeta2^4
         -4*[zeta5,3]
         );



      id HPL(1,0,1,1,1,0,0,0)= (
         +9523/21000*zeta2^4
         +1/2*zeta2*zeta3^2
         -zeta3*zeta5
         +6/5*[zeta5,3]
         );



      id HPL(1,0,1,1,1,0,0,1)= (
         -841/2625*zeta2^4
         +zeta2*zeta3^2
         +2*zeta3*zeta5
         +9/5*[zeta5,3]
         );



      id HPL(1,0,1,1,1,0,1,0)= (
         -383/525*zeta2^4
         -zeta2*zeta3^2
         +zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,0,1,1,1,0,1,1)= (
         +18/35*zeta2^4
         -4*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,0,1,1,1,1,0,0)= (
         +64/125*zeta2^4
         +1/2*zeta2*zeta3^2
         -1/2*zeta3*zeta5
         -7/10*[zeta5,3]
         );



      id HPL(1,0,1,1,1,1,0,1)= (
         -326/175*zeta2^4
         +8*zeta3*zeta5
         );



      id HPL(1,0,1,1,1,1,1,0)= (
         +178/125*zeta2^4
         -4*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(1,0,1,1,1,1,1,1)= (
         +24/25*zeta2^4
         );



      id HPL(1,1,0,0,0,0,0,0)= (
         -6/35*zeta2^4
         +zeta3*zeta5
         );



      id HPL(1,1,0,0,0,0,0,1)= (
         -281/875*zeta2^4
         -1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,1,0,0,0,0,1,0)= (
         +64/125*zeta2^4
         +1/2*zeta2*zeta3^2
         -1/2*zeta3*zeta5
         -7/10*[zeta5,3]
         );



      id HPL(1,1,0,0,0,0,1,1)= (
         -1/200*zeta2^4
         -zeta2*zeta3^2
         +2*zeta3*zeta5
         );



      id HPL(1,1,0,0,0,1,0,0)= (
         -262/525*zeta2^4
         -zeta2*zeta3^2
         -1/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(1,1,0,0,0,1,0,1)= (
         +6611/21000*zeta2^4
         +3/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -21/10*[zeta5,3]
         );



      id HPL(1,1,0,0,0,1,1,0)= (
         -719/600*zeta2^4
         -3/2*zeta2*zeta3^2
         +19/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(1,1,0,0,0,1,1,1)= (
         -283/175*zeta2^4
         +zeta2*zeta3^2
         +15/2*zeta3*zeta5
         +3/2*[zeta5,3]
         );



      id HPL(1,1,0,0,1,0,0,0)= (
         +22/175*zeta2^4
         +1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         +5/2*[zeta5,3]
         );



      id HPL(1,1,0,0,1,0,0,1)= (
         -9757/21000*zeta2^4
         -1/2*zeta2*zeta3^2
         +4*zeta3*zeta5
         +27/10*[zeta5,3]
         );



      id HPL(1,1,0,0,1,0,1,0)= (
         +37603/21000*zeta2^4
         +zeta2*zeta3^2
         -27/2*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,1,0,0,1,0,1,1)= (
         +2487/875*zeta2^4
         -3*zeta2*zeta3^2
         -11*zeta3*zeta5
         -23/10*[zeta5,3]
         );



      id HPL(1,1,0,0,1,1,0,0)= (
         +13/1400*zeta2^4
         +zeta2*zeta3^2
         -zeta3*zeta5
         );



      id HPL(1,1,0,0,1,1,0,1)= (
         +6/35*zeta2^4
         +5/2*zeta2*zeta3^2
         -6*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,1,0,0,1,1,1,0)= (
         +44/105*zeta2^4
         -2*zeta3*zeta5
         );



      id HPL(1,1,0,0,1,1,1,1)= (
         +156/175*zeta2^4
         -5*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,1,0,1,0,0,0,0)= (
         -2266/2625*zeta2^4
         -zeta2*zeta3^2
         +6*zeta3*zeta5
         -7/10*[zeta5,3]
         );



      id HPL(1,1,0,1,0,0,0,1)= (
         -4573/21000*zeta2^4
         -2*zeta2*zeta3^2
         +7*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(1,1,0,1,0,0,1,0)= (
         -22453/21000*zeta2^4
         -zeta2*zeta3^2
         +2*zeta3*zeta5
         -27/10*[zeta5,3]
         );



      id HPL(1,1,0,1,0,0,1,1)= (
         -2592/875*zeta2^4
         +17*zeta3*zeta5
         +23/10*[zeta5,3]
         );



      id HPL(1,1,0,1,0,1,0,0)= (
         -8927/7000*zeta2^4
         -4*zeta2*zeta3^2
         +22*zeta3*zeta5
         +18/5*[zeta5,3]
         );



      id HPL(1,1,0,1,0,1,0,1)= (
         -31/175*zeta2^4
         -2*zeta2*zeta3^2
         +4*zeta3*zeta5
         );



      id HPL(1,1,0,1,0,1,1,0)= (
         -487/175*zeta2^4
         -4*zeta2*zeta3^2
         +51/2*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,1,0,1,0,1,1,1)= (
         -582/175*zeta2^4
         +20*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,1,0,1,1,0,0,0)= (
         -97/4200*zeta2^4
         +1/2*zeta2*zeta3^2
         -7/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(1,1,0,1,1,0,0,1)= (
         +428/875*zeta2^4
         -1/2*zeta2*zeta3^2
         -11/2*zeta3*zeta5
         -17/10*[zeta5,3]
         );



      id HPL(1,1,0,1,1,0,1,0)= (
         +552/875*zeta2^4
         +2*zeta2*zeta3^2
         +zeta3*zeta5
         -13/10*[zeta5,3]
         );



      id HPL(1,1,0,1,1,0,1,1)= (
         -18/25*zeta2^4
         +6*zeta3*zeta5
         );



      id HPL(1,1,0,1,1,1,0,0)= (
         -262/525*zeta2^4
         -zeta2*zeta3^2
         -1/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(1,1,0,1,1,1,0,1)= (
         +562/175*zeta2^4
         -12*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(1,1,0,1,1,1,1,0)= (
         -92/35*zeta2^4
         +6*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,1,0,1,1,1,1,1)= (
         -72/25*zeta2^4
         );



      id HPL(1,1,1,0,0,0,0,0)= (
         +61/175*zeta2^4
         +1/2*zeta2*zeta3^2
         -3*zeta3*zeta5
         );



      id HPL(1,1,1,0,0,0,0,1)= (
         +881/7000*zeta2^4
         +zeta2*zeta3^2
         -7/2*zeta3*zeta5
         -3/10*[zeta5,3]
         );



      id HPL(1,1,1,0,0,0,1,0)= (
         +9523/21000*zeta2^4
         +1/2*zeta2*zeta3^2
         -zeta3*zeta5
         +6/5*[zeta5,3]
         );



      id HPL(1,1,1,0,0,0,1,1)= (
         +58/35*zeta2^4
         -19/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(1,1,1,0,0,1,0,0)= (
         -97/4200*zeta2^4
         +1/2*zeta2*zeta3^2
         -7/2*zeta3*zeta5
         -3/2*[zeta5,3]
         );



      id HPL(1,1,1,0,0,1,0,1)= (
         -5119/2625*zeta2^4
         +zeta2*zeta3^2
         +10*zeta3*zeta5
         +11/5*[zeta5,3]
         );



      id HPL(1,1,1,0,0,1,1,0)= (
         +79/175*zeta2^4
         +1/2*zeta2*zeta3^2
         -9/2*zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,1,1,0,0,1,1,1)= (
         -2/25*zeta2^4
         );



      id HPL(1,1,1,0,1,0,0,0)= (
         +18259/21000*zeta2^4
         +2*zeta2*zeta3^2
         -8*zeta3*zeta5
         +3/5*[zeta5,3]
         );



      id HPL(1,1,1,0,1,0,0,1)= (
         +4483/2625*zeta2^4
         +zeta2*zeta3^2
         -9*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(1,1,1,0,1,0,1,0)= (
         +1307/875*zeta2^4
         +2*zeta2*zeta3^2
         -19*zeta3*zeta5
         -9/5*[zeta5,3]
         );



      id HPL(1,1,1,0,1,0,1,1)= (
         +666/175*zeta2^4
         -24*zeta3*zeta5
         -4*[zeta5,3]
         );



      id HPL(1,1,1,0,1,1,0,0)= (
         +22/175*zeta2^4
         +1/2*zeta2*zeta3^2
         +2*zeta3*zeta5
         +5/2*[zeta5,3]
         );



      id HPL(1,1,1,0,1,1,0,1)= (
         -478/175*zeta2^4
         +8*zeta3*zeta5
         +4*[zeta5,3]
         );



      id HPL(1,1,1,0,1,1,1,0)= (
         +426/175*zeta2^4
         -4*zeta3*zeta5
         );



      id HPL(1,1,1,0,1,1,1,1)= (
         +24/5*zeta2^4
         );



      id HPL(1,1,1,1,0,0,0,0)= (
         -499/1400*zeta2^4
         -zeta2*zeta3^2
         +4*zeta3*zeta5
         );



      id HPL(1,1,1,1,0,0,0,1)= (
         -96/125*zeta2^4
         -1/2*zeta2*zeta3^2
         +9/2*zeta3*zeta5
         +3/10*[zeta5,3]
         );



      id HPL(1,1,1,1,0,0,1,0)= (
         -293/2625*zeta2^4
         -zeta2*zeta3^2
         +9/2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,1,1,1,0,0,1,1)= (
         -156/175*zeta2^4
         +6*zeta3*zeta5
         +[zeta5,3]
         );



      id HPL(1,1,1,1,0,1,0,0)= (
         -2266/2625*zeta2^4
         -zeta2*zeta3^2
         +6*zeta3*zeta5
         -7/10*[zeta5,3]
         );



      id HPL(1,1,1,1,0,1,0,1)= (
         -94/175*zeta2^4
         +8*zeta3*zeta5
         );



      id HPL(1,1,1,1,0,1,1,0)= (
         -8/7*zeta2^4
         +zeta3*zeta5
         -[zeta5,3]
         );



      id HPL(1,1,1,1,0,1,1,1)= (
         -24/5*zeta2^4
         );



      id HPL(1,1,1,1,1,0,0,0)= (
         +61/175*zeta2^4
         +1/2*zeta2*zeta3^2
         -3*zeta3*zeta5
         );



      id HPL(1,1,1,1,1,0,0,1)= (
         +58/125*zeta2^4
         -4*zeta3*zeta5
         -2/5*[zeta5,3]
         );



      id HPL(1,1,1,1,1,0,1,0)= (
         +494/875*zeta2^4
         -2*zeta3*zeta5
         +2/5*[zeta5,3]
         );



      id HPL(1,1,1,1,1,0,1,1)= (
         +72/25*zeta2^4
         );



      id HPL(1,1,1,1,1,1,0,0)= (
         -6/35*zeta2^4
         +zeta3*zeta5
         );



      id HPL(1,1,1,1,1,1,0,1)= (
         -24/25*zeta2^4
         );



      id HPL(1,1,1,1,1,1,1,0)= (
         +24/175*zeta2^4
         );

