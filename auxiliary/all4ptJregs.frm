        Format 250;
        Off statistics;

        #include- pss2-polylogs.h

#procedure IntegrateOrder(i)

	id order('i') = Int('i')*KN('Npts');
        #call IntegrationOrdering()
* According to Table 1 of 1609.07078, expanding the KN factor
* up to weight 6 computes the regular 4pt integrals up to zeta7.
* You can select up to weight 6 here. If you want more, you'll
* need to generate z-removal identities at higher length.
        #call KobaNielsen('Npts',6)
        #call IntegrateG(z'i')

#endprocedure

#procedure Sliced(i)

        transform Z replace(1,last)=(1,z1,2,z2,3,z3,4,z4,5,z5,6,z6,7,z7,8,z8);

        argument Z;
        id z1 = 0;
        id z{'Npts'-1} = 1;
        endargument;
        .sort

        #call IntegrateOrder('i')

        .sort
        Polyfun;
        id gather(x1?) = x1;

        bracket zeta2,zeta3,zeta5,zeta7;
        print +s -f;
        .sort
        drop;
        .sort

#endprocedure

        #define Npts "4"

        L [Z(1,2)] = order(2)*Z(1,2);
        L [Z(2,3)] = order(2)*Z(2,3);
        #call Sliced(2)

        .end
