# Python script to modify the NptJregs.h files to avoid loading
# the terms containing zeta's of weight higher than specified in
# the call to BGexpand(weight)

# This is how it was used:
# python add_conditionals.py 6ptJregs.h > 6ptJregs-new.h

# After deciding 6ptJregs-new.h is OK, rename it to 6ptJregs.h etc

import fileinput
import re

count3, count4, count5, count6, count7 = 0,0,0,0,0
replaced = 0

for line in fileinput.input():

# reset counters for each new expression
    match = re.search(r'=', line)
    if match:
        count3, count4, count5, count6, count7 = 0,0,0,0,0
        replaced = 0

    match = re.search(r'tmpF\(3\)', line)
    if match:
        if count3 == 0:
            line = re.sub(r'\+ tmpF\(3\)\*',r"#if 'w' >= 3\n\t + ", line.rstrip())
            count3 = count3 + 1
            replaced = replaced + 1
        else:
            line = re.sub(r'\+ tmpF\(3\)\*',r"+ ", line.rstrip())

    match = re.search(r'tmpF\(4\)', line)
    if match:
        if count4 == 0:
            line = re.sub(r'\+ tmpF\(4\)\*',r"#if 'w' >= 4\n\t + ", line.rstrip())
            count4 = count4 + 1
            replaced = replaced + 1
        else:
            line = re.sub(r'\+ tmpF\(4\)\*',r"+ ", line.rstrip())

    match = re.search(r'tmpF\(5\)', line)
    if match:
        if count5 == 0:
            line = re.sub(r'\+ tmpF\(5\)\*',r"#if 'w' >= 5\n\t + ", line.rstrip())
            count5 = count5 + 1
            replaced = replaced + 1
        else:
            line = re.sub(r'\+ tmpF\(5\)\*',r"+ ", line.rstrip())

    match = re.search(r'tmpF\(6\)', line)
    if match:
        if count6 == 0:
            line = re.sub(r'\+ tmpF\(6\)\*',r"#if 'w' >= 6\n\t + ", line.rstrip())
            count6 = count6 + 1
            replaced = replaced + 1
        else:
            line = re.sub(r'\+ tmpF\(6\)\*',r"+ ", line.rstrip())

    match = re.search(r'tmpF\(7\)', line)
    if match:
        if count7 == 0:
            line = re.sub(r'\+ tmpF\(7\)\*',r"#if 'w' >= 7\n\t + ", line.rstrip())
            count7 = count7 + 1
            replaced = replaced + 1
        else:
            line = re.sub(r'\+ tmpF\(7\)\*',r"+ ", line.rstrip())

# We need to write a matching number of #endif's at the end of the expression
    match = re.search(r';', line)
    if match:
        if replaced == 1:
            line = re.sub(r';',r"\n\t#endif\n\t;", line.rstrip())
        if replaced == 2:
            line = re.sub(r';',r"\n\t#endif\n\t#endif\n\t;", line.rstrip())
        if replaced == 3:
            line = re.sub(r';',r"\n\t#endif\n\t#endif\n\t#endif\n\t;", line.rstrip())
        if replaced == 4:
            line = re.sub(r';',r"\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t;", line.rstrip())
        if replaced == 5:
            line = re.sub(r';',r"\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t;", line.rstrip())
        if replaced == 6:
            line = re.sub(r';',r"\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t#endif\n\t;", line.rstrip())

    print(line.rstrip())
