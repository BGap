
   id Jpt4reg1(?m1)*Jpt4reg1(?m2)*Jpt4reg1(?m3) =

       + zeta2*zwgt^2 * (
          - KK(?m3,[c],?m2)
          )

          #if 'w' >= 3
       + zeta3*zwgt^3 * (
          + KK(?m2,[c],?m1)*KK(?m3,[c],?m2)
          + KK(?m3,[c],?m2)^2
          )

          #if 'w' >= 4
       + zeta2^2*zwgt^4 * (
          - 2/5*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)
          - 1/10*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^2
          - 2/5*KK(?m3,[c],?m2)^3
          )

          #if 'w' >= 5
       + zeta2*zeta3*zwgt^5 * (
          - KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^2
          - KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^3
          )

       + zeta5*zwgt^5 * (
          + KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)
          + 2*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^2
          + 2*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^3
          + KK(?m3,[c],?m2)^4
          )

          #if 'w' >= 6
       + zeta2^3*zwgt^6 * (
          - 8/35*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)
          - 6/35*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^2
          - 23/70*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^3
          - 6/35*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^4
          - 8/35*KK(?m3,[c],?m2)^5
          )

       + zeta3^2*zwgt^6 * (
          + 1/2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^2
          + KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^3
          + 1/2*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^4
          )

          #if 'w' >= 7
       + zeta2^2*zeta3*zwgt^7 * (
          - 2/5*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          - 1/2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          - 1/2*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          - 2/5*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^5
          )

       + zeta2*zeta5*zwgt^7 * (
          - KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          - 2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          - 2*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          - KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^5
          )

       + zeta7*zwgt^7 * (
          + KK(?m2,[c],?m1)^5*KK(?m3,[c],?m2)
          + 3*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          + 5*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          + 5*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          + 3*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^5
          + KK(?m3,[c],?m2)^6
          )
          #endif
          #endif
          #endif
          #endif
          #endif
          ;

   id Jpt4reg2(?m1)*Jpt4reg2(?m2)*Jpt4reg2(?m3) =

       + zeta2*zwgt^2 * (
          - KK(?m2,[c],?m1)
          )

          #if 'w' >= 3
       + zeta3*zwgt^3 * (
          + KK(?m2,[c],?m1)^2
          + KK(?m2,[c],?m1)*KK(?m3,[c],?m2)
          )

          #if 'w' >= 4
       + zeta2^2*zwgt^4 * (
          - 2/5*KK(?m2,[c],?m1)^3
          - 1/10*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)
          - 2/5*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^2
          )

          #if 'w' >= 5
       + zeta2*zeta3*zwgt^5 * (
          - KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)
          - KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^2
          )

       + zeta5*zwgt^5 * (
          + KK(?m2,[c],?m1)^4
          + 2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)
          + 2*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^2
          + KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^3
          )

          #if 'w' >= 6
       + zeta2^3*zwgt^6 * (
          - 8/35*KK(?m2,[c],?m1)^5
          - 6/35*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)
          - 23/70*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^2
          - 6/35*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^3
          - 8/35*KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^4
          )

       + zeta3^2*zwgt^6 * (
          + 1/2*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)
          + KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^2
          + 1/2*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^3
          )

          #if 'w' >= 7
       + zeta2^2*zeta3*zwgt^7 * (
          - 2/5*KK(?m2,[c],?m1)^5*KK(?m3,[c],?m2)
          - 1/2*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          - 1/2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          - 2/5*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          )

       + zeta2*zeta5*zwgt^7 * (
          - KK(?m2,[c],?m1)^5*KK(?m3,[c],?m2)
          - 2*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          - 2*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          - KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          )

       + zeta7*zwgt^7 * (
          + KK(?m2,[c],?m1)^6
          + 3*KK(?m2,[c],?m1)^5*KK(?m3,[c],?m2)
          + 5*KK(?m2,[c],?m1)^4*KK(?m3,[c],?m2)^2
          + 5*KK(?m2,[c],?m1)^3*KK(?m3,[c],?m2)^3
          + 3*KK(?m2,[c],?m1)^2*KK(?m3,[c],?m2)^4
          + KK(?m2,[c],?m1)*KK(?m3,[c],?m2)^5
          )
          #endif
          #endif
          #endif
          #endif
          #endif
          ;


