* This file implements the Berends-Giele recursion to compute the
* alpha' expansion of disk integrals using the method described in the
* paper arXiv:1609.07078
*
* Non-abelian Z-theory: Berends-Giele recursion for the alpha' expansion
* of disk integrals
* 
* by Carlos R. Mafra and Oliver Schlotterer.
*
* You can use this file as you wish, as long as you cite the
* paper arXiv:1609.07078 afterwards. In addition, consider citing
* the paper arXiv:1603.09731 as well, as it is very closely related
* 
* Copyright (c) 2016 Carlos R. Mafra

	Indices a,b,c,d,e,f,g,h,i,j,k,l;
	Indices m,n,p,q,r,ss,t,u,v,x;
	Indices <m1,n1>,...,<m20,n20>;
	Indices i1,...,i20;
	Indices j1,...,j20;

        Symbols [p],[c];
        Symbol x1;
        Symbols <zeta2>,...,<zeta10>;

* Mandelstam s(i,j), Is(i,j)=1/s(i,j), KK(i,[c],j) = ki.kj
        CFunction s(s), Is(s), KK;

        CFunction phi, Zint;
        CFunction nargs;

        CFunction ctmpF1,countL,countR;
        Function tmpF, tmpF1;

* phi(i1,i2,i3|j1,j2,j3) = ML(i1)*ML(i2)*ML(i3)*MR(j1)*MR(j2)*MR(j3)
        Function ML,MR,L,R;
        Function KKL,KKR;

	Autodeclare Function Jpt;

#procedure DeconcatenateN(F,n)
*
* Deconcatenate function F into n-partitions or all partitions when n==0
*
* n==0: F(1,2,3) --> F(1)*F(2)*F(3) + F(1,2)*F(3) + F(1)*F(2,3)
* n==2: F(1,2,3) --> F(1,2)*F(3) + F(1)*F(2,3)
*
	if (match(tmpF1(?m)));
		print "Error: temp function already present: %t";
		exit;
	endif;

	if ((count('F',1) <= -1) || (count('F',1) >= 2));
		print "Error: Deconc does not work for : %t";
                exit;
        endif;

        chainout 'F';

	repeat id once 'F'(?m)*'F'(?n) =
        + 'F'(?m,?n)
        + tmpF1(?m)*'F'(?n)
        ;

        id tmpF1(?m) = 'F'(?m);

        #if 'n' == 0
* do nothing, keep all partitions
        #else
* keep only n-partitions (or up to n-partitions)
*        if ((match('F'(?m))) && (count('F',1) != 'n'));
        if ((match('F'(?m))) && (count('F',1) > 'n'));
		discard;
	endif;
	#endif

* remove trivial partition F(1,2,3) -> F(1,2,3)
	if ((count('F',1) == 1));
		discard;
	endif;

#endprocedure


#procedure BiDeconcatenate(phi,n)
*
* phi(A,[p],B) --> all deconcatenations of A and B, separated by [c]
* 
* s(1,2,3)*phi(1,2,3,[p],4,5,6) = phi(1,[c],2,3,[p],4,[c],5,6) + phi(1,2,[c],3,[p],4,[c],5,6) + ...

* A and B cannot be composed of smaller words, so no [c]s allowed
	if (match(phi(?m,[c],?n)));
        	print "Bug: cannot deconcatenate multiple slots %t";
        	exit;
        endif;

        id once 'phi'(?m1,[p],?m2) = Is(?m1)*ML(?m1)*MR(?m2);
        #call DeconcatenateN(ML,'n')
        #call DeconcatenateN(MR,'n')

* keep only the same depth of deconcatenation on both sides
        if (count(ML,1) != count(MR,1));
        discard;
        endif;

* Kill deconcatenations where there is no matching number of slots
* between ML() and MR(). Recall that ML() gives rise to the A-slots
* in the various BGphiN.h files and MR() the B-slots. These slots
* refer to the definition (3.13) of arXiv:1609.07078v1, namely
* T_{A1 A2...}^{B1 B2...}. If |Ai| != |Bj| for all i,j it means
* that all resulting factors phi(Ai|Bj) vanish by the initial
* condition from eq.(3.3).
        id ML(?m) = ML(?m)*countL(nargs_(?m));
        id MR(?m) = MR(?m)*countR(nargs_(?m));
        id countL(i?)*countR(i?) = 1;
* if any countL() remains, it means there was no matching countR(),
* so kill the term
        id countL(i?) = 0;

        .sort:bideconc'n';
#endprocedure


#procedure tLalphaExpand(n,w)

        #if ({'$Npts'-1-'n'} >= 9 && 'w' >= 7)
        #include- BGphiN/BGphi9-Jreg.h
        bracket L,R;
        .sort:brack9;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*L(?a8)*L(?a9)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)*R(?b8)*R(?b9) =
        + phi(?a9,[p],?b9)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*L(?a8)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)*R(?b8)
        - phi(?a9,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*L(?a8)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)*R(?b8)*R(?b9)
        ;
        #call MultiparticleConstraint(9)
        #endif

        #if ({'$Npts'-1-'n'} >= 8 && 'w' >= 6)
        #include- BGphiN/BGphi8-Jreg.h
        bracket L,R;
        .sort:brack8;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*L(?a8)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)*R(?b8) =
        + phi(?a8,[p],?b8)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)
        - phi(?a8,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)*R(?b8)
        ;
        #call MultiparticleConstraint(8)
        #endif

        #if ({'$Npts'-1-'n'} >= 7 && 'w' >= 5)
        #include- BGphiN/BGphi7-Jreg.h
        bracket L,R;
        .sort:brack7;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*L(?a7)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7) =
        + phi(?a7,[p],?b7)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)
        - phi(?a7,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)*R(?b7)
        ;
        #call MultiparticleConstraint(7)
        #endif

        #if ({'$Npts'-1-'n'} >= 6 && 'w' >= 4)
        #include- BGphiN/BGphi6-Jreg.h
        bracket L,R;
        .sort:brack6;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*L(?a6)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6) =
        + phi(?a6,[p],?b6)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5)
        - phi(?a6,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*R(?b2)*R(?b3)*R(?b4)*R(?b5)*R(?b6)
        ;
        #call MultiparticleConstraint(6)
        #endif

        #if ({'$Npts'-1-'n'} >= 5 && 'w' >= 3)
        #include- BGphiN/BGphi5-Jreg.h
        bracket L,R;
        .sort:brack5;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*L(?a5)*R(?b1)*R(?b2)*R(?b3)*R(?b4)*R(?b5) =
        + phi(?a5,[p],?b5)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*R(?b1)*R(?b2)*R(?b3)*R(?b4)
        - phi(?a5,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*L(?a4)*R(?b2)*R(?b3)*R(?b4)*R(?b5)
        ;
        #call MultiparticleConstraint(5)
        #endif

        #if {'$Npts'-1-'n'} >= 4
        #include- BGphiN/BGphi4-Jreg.h
        bracket L,R;
        .sort:brack4;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*L(?a4)*R(?b1)*R(?b2)*R(?b3)*R(?b4) =
        + phi(?a4,[p],?b4)*L(?a1)*L(?a2)*L(?a3)*R(?b1)*R(?b2)*R(?b3)
        - phi(?a4,[p],?b1)*L(?a1)*L(?a2)*L(?a3)*R(?b2)*R(?b3)*R(?b4)
        ;
        #call MultiparticleConstraint(4)
        #endif

        #if {'$Npts'-1-'n'} >= 3
        #include- BGphiN/BGphi3-Jreg.h
        bracket L,R;
        .sort:brack3;
        keep brackets;
        id L(?a1)*L(?a2)*L(?a3)*R(?b1)*R(?b2)*R(?b3) =
        + phi(?a3,[p],?b3)*L(?a1)*L(?a2)*R(?b1)*R(?b2)
        - phi(?a3,[p],?b1)*L(?a1)*L(?a2)*R(?b2)*R(?b3)
        ;
        #call MultiparticleConstraint(3)
        #endif

        bracket L,R;
        .sort:brack2;
        keep brackets;
        id L(?a1)*L(?a2)*R(?b1)*R(?b2) =
        + phi(?a2,[p],?b2)*phi(?a1,[p],?b1)
        - phi(?a2,[p],?b1)*phi(?a1,[p],?b2)
        ;
        #call MultiparticleConstraint(2)

        id ML(?m1)*ML(?m2)*MR(?n1)*MR(?n2) =
	+ phi(?m1,[p],?n1)*phi(?m2,[p],?n2)
        - phi(?m2,[p],?n1)*phi(?m1,[p],?n2)
        ;

        bracket phi;
        .sort:brackphi;
        keep brackets;

        #call MultiparticleConstraint(1)

#endprocedure


#procedure MultiparticleConstraint(n)

        if (match(phi(?m,[c],?n)));
        print "bug: no subwords in phi %t";
        exit;
        endif;

        id phi(i?,[p],j?) = delta_(i,j);
        id phi(i?,j?,[p],i?,j?) = Is(i,j);
        id phi(i?,j?,[p],j?,i?) = - Is(i,j);

* remove phi(A|B) where length(A) != length(B)
        id phi(?m,[p],?n) = phi(?m,[p],?n)*delta_(nargs_(?m),nargs_(?n));

* remove phi(A|B) when letters in A and B are not the same
        if (match(ctmpF1(?m))==0);
        id phi(?m,[p],?n) = phi(?m,[p],?n)*ctmpF1(?m,[p],?n);
        repeat id ctmpF1(?m1,i?,?m2,[p],?m3,i?,?m4) = ctmpF1(?m1,?m2,[p],?m3,?m4);
        id ctmpF1([p]) = 1;
        id ctmpF1(?m) = 0;
        endif;

        .sort: MulCtrnt'n';

#endprocedure

#procedure BGexpand(w)

        .sort:AlphapWeight'w';
        Symbol zwgt(:'w');

        #if 'w' > 7
        print "Error: order higher than alpha^7 not implemented!";
        exit;
        #endif

* Sanity check: number of arguments on both sides must be equal
        id Zint(?m,[p],?n) = Zint(?m,[p],?n)*nargs(nargs_(?m) - nargs_(?n));
        id nargs(0) = 1;
        if (match(nargs(i?)));
        print "Error: unequal number of labels in domain and integrand: %t";
        exit;
        endif;

        if (match(Zint(?m,[p],?n)) == 0);
        print "Error: specify the domain P and integrand Q as Zint(P,[p],Q)";
        exit;
        endif;

* Get the number of points
        id once Zint(?m,[p],?n) = Zint(?m,[p],?n)*nargs(nargs_(?n));
        id once nargs(x1?$Npts) = 1;

        #define Npts "$Npts"

* Exploit cyclic symmetry of Zint(P|Q) in Q to match last label
        id once Zint(?m,i?,[p],?n,i?,?p) = s(?n,?p)*phi(?m,[p],?p,?n);
        .sort

* TODO: the number of calls to BiDeconcatenate(phi,i) should vary according to w
        #call BiDeconcatenate(phi,{'w'+2})
        #call CancelMandelstams('Npts')

        #call tLalphaExpand(0,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(1,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(2,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(3,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(4,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(5,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(6,'w')
        #call BiDeconcatenate(phi,{'w'+2})
        #call tLalphaExpand(7,'w')

        if (match(phi(?m)));
        print "Bug: cannot have phi() at the end of recursion: %t";
        print "Add more calls to tLalphaExpand()";
        exit;
        endif;

        #if ('w' >= 7 && '$Npts' >= 10)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack10;
        keep brackets;
        #include- NptJregs/10ptJregs.h
        #endif

        #if ('w' >= 6 && '$Npts' >= 9)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack9;
        keep brackets;
        #include- NptJregs/9ptJregs.h
        #endif

        #if ('w' >= 5 && '$Npts' >= 8)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack8;
        keep brackets;
        #include- NptJregs/8ptJregs.h
        #endif

        #if ('w' >= 4 && '$Npts' >= 7)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack7;
        keep brackets;
        #include- NptJregs/7ptJregs.h
        #endif

        #if ('w' >= 3 && '$Npts' >= 6)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack6;
        keep brackets;
        #include- NptJregs/6ptJregs.h
        #endif

        #if ('w' >= 2 && '$Npts' >= 5)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack5;
        keep brackets;
        #include- NptJregs/5ptJregs.h
        #endif

        #if ('w' >= 1 && '$Npts' >= 4)
        abracket phi,Is,KK,zeta2,zeta3,zeta5,zeta7;
        .sort:abrack4;
        keep brackets;
        #include- NptJregs/4ptJregs.h
        #endif

        id zwgt = 1;
        #call KKTos()

#endprocedure

#procedure KKTos()

        bracket KK;
        .sort:bracKK;
        keep brackets;

        repeat id KK(?n,i?,j?,[c],?m) = KK(?n,i,[c],?m) + KK(j,[c],?m);

        id KK(i?,[c],i1?) = s(i,i1);
        id KK(i?,[c],i1?,i2?) = s(i,i1) + s(i,i2);
        id KK(i?,[c],i1?,i2?,i3?) = s(i,i1) + s(i,i2) + s(i,i3);
        id KK(i?,[c],i1?,i2?,i3?,i4?) = s(i,i1) + s(i,i2) + s(i,i3) + s(i,i4);
        id KK(i?,[c],i1?,i2?,i3?,i4?,i5?) = s(i,i1) + s(i,i2) + s(i,i3) + s(i,i4) + s(i,i5);
        id KK(i?,[c],i1?,...,i6?) = s(i,i1) + s(i,i2) + s(i,i3) + s(i,i4) + s(i,i5) + s(i,i6);
        id KK(i?,[c],i1?,...,i7?) = s(i,i1) + s(i,i2) + s(i,i3) + s(i,i4) + s(i,i5) + s(i,i6) + s(i,i7);
        id KK(i?,[c],i1?,...,i8?) = s(i,i1) + s(i,i2) + s(i,i3) + s(i,i4) + s(i,i5) + s(i,i6) + s(i,i7) + s(i,i8);

        .sort:KKTos;
*        #call KNBasis('$Npts')
*        #call MomentumConservationMandelstams('$Npts')
*        #call NumericMandelstams('$Npts')

#endprocedure


#ifndef 'pss2mandelstams'

#procedure CancelMandelstams(N)

        #do i=2,'N'
        id s(i1?,...,i'i'?)^-1 = Is(i1,...,i'i');
        id s(i1?,...,i'i'?)*Is(i1?,...,i'i'?) = 1;
	#enddo

#endprocedure

#endif
